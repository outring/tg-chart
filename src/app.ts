import {Chart, IChartData} from "./chart/chart.js";
import {ChartView} from "./chart/chartView.js";
import {formatAsMonthAndDay} from "./chart/date.js";
import {addClassName, createElement, removeClassName} from "./chart/dom.js";
import {ChartNavigator} from "./chart/chartNavigator.js";
import {ChartSwitch} from "./chart/chartSwitch.js";
import {themes, UiMode, uiModeNames} from "./uiMode.js";

let activeUiMode: UiMode = "day";
let activeTheme = themes[activeUiMode];

const xhr = new XMLHttpRequest();
xhr.onreadystatechange = function () {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        render(JSON.parse(xhr.responseText));
    }
};
xhr.open("GET", "./chart_data.json", true);
xhr.send(null);

function render(charts: IChartData<number>[]): void {
    const container = document.querySelector<HTMLElement>(".app")!;

    const chartComponents = charts.map((x, i) => renderChart(container, `Chart ${i + 1}`, x));

    renderThemeSwitch(chartComponents);
}

export function renderThemeSwitch(chartComponents: [ChartView<Date>, ChartNavigator][]): void {
    const link = createElement("a", {href: "javascript:void(0)", className: "themeSwitch-link"});
    const panel = createElement("div", {className: "themeSwitch"}, [link]);

    link.addEventListener("click", () => {
        setUiMode(link, chartComponents, activeUiMode === "day" ? "night" : "day");
    });

    setUiMode(link, chartComponents, activeUiMode);

    document.body.appendChild(panel);
}

function renderChart(appContainer: HTMLElement, chartName: string, chartData: IChartData<number>): [ChartView<Date>, ChartNavigator] {
    const container = createElement("div", {className: "chart"}, [
        createElement("div", {className: "chart-heading"}, [chartName])
    ]);
    appContainer.appendChild(container);

    const chart = Chart.fromData(chartData, x => new Date(x));

    const view = new ChartView<Date>({
        gridDensityX: 0.3,
        gridDensityY: 0.2,
        formatLabelX: formatAsMonthAndDay
    }, activeTheme.view);
    view.appendTo(container);
    view.setChart(chart);

    const navigator = new ChartNavigator(activeTheme.navigator);
    navigator.appendTo(container);
    navigator.setChart(chart);
    navigator.addView(view);

    const chartSwitch = new ChartSwitch();
    chartSwitch.appendTo(container);
    chartSwitch.setChart(chart);
    chartSwitch.addView(view);
    chartSwitch.addView(navigator);

    return [view, navigator];
}

function setUiMode(link: HTMLElement, chartComponents: [ChartView<Date>, ChartNavigator][], uiMode: UiMode): void {
    addClassName(document.documentElement, "theme");
    removeClassName(document.documentElement, "theme_" + activeUiMode);
    addClassName(document.documentElement, "theme_" + uiMode);

    activeUiMode = uiMode;
    activeTheme = themes[activeUiMode];

    link.innerText = `Switch to ${uiModeNames[activeUiMode === "day" ? "night" : "day"]}`;

    setTimeout(() => {
        chartComponents.forEach(x => {
            x[0].setStyle(activeTheme.view);
            x[1].setStyle(activeTheme.navigator);
        });
    }, 100);
}
