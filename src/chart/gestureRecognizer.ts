type NativeEvent = PointerEvent | TouchEvent | MouseEvent;
type NativeEventType = "PointerEvent" | "TouchEvent" | "MouseEvent";

interface IEventMap {
    start: keyof HTMLElementEventMap;
    progress: keyof HTMLElementEventMap;
    end: keyof HTMLElementEventMap;
}

const eventTypeNames: NativeEventType[] = ["PointerEvent", "TouchEvent", "MouseEvent"];

const eventsMaps: { [type in NativeEventType]: IEventMap } = {
    PointerEvent: {
        start: "pointerdown",
        progress: "pointermove",
        end: "pointerup"
    },
    TouchEvent: {
        start: "touchstart",
        progress: "touchmove",
        end: "touchend"
    },
    MouseEvent: {
        start: "mousedown",
        progress: "mousemove",
        end: "mouseup"
    }
};
const eventTypeName = eventTypeNames.filter(x => x in window)[0];
const eventsMap = eventsMaps[eventTypeName];

function isValidEvent(e: NativeEvent): boolean {
    if (eventTypeName === "PointerEvent") {
        const pointerEvent = <PointerEvent>e;
        return pointerEvent.isPrimary;
    }

    if (eventTypeName === "TouchEvent") {
        const touchEvent = <TouchEvent>e;
        return touchEvent.changedTouches.length > 0;
    }

    return true;
}

function getEventXY(e: NativeEvent): [number, number] {
    if (eventTypeName === "PointerEvent") {
        const pointerEvent = <PointerEvent>e;
        return [pointerEvent.clientX, pointerEvent.clientY];
    }

    if (eventTypeName === "TouchEvent") {
        const touchEvent = <TouchEvent>e;
        return [touchEvent.changedTouches[0].clientX, touchEvent.changedTouches[0].clientY];
    }

    const mouseEvent = <MouseEvent>e;
    return [mouseEvent.clientX, mouseEvent.clientY];
}

type GestureType = "tap" | "pan";
type GestureState = "start" | "end" | "indefinite";

export interface IGestureEvent {
    type: GestureType;
    x: number;
    y: number;
    state: GestureState;
    elementWidth: number;
    elementHeight: number;
}

type GestureHandler = (event: Readonly<IGestureEvent>) => boolean;

const gestureTapTolerance = 5;

const gestureEvent: IGestureEvent = {
    type: "tap",
    x: 0,
    y: 0,
    state: "indefinite",
    elementWidth: 0,
    elementHeight: 0
};

export class GestureRecognizer {

    private _element: HTMLElement;

    private _handlers: { [type: string]: GestureHandler[] } = {};

    private _currentGesture: GestureType | undefined;

    private _initialXY = [0, 0];
    private _elementRect: ClientRect | DOMRect;

    constructor(element: HTMLElement) {
        this._element = element;

        this._elementRect = this._element.getBoundingClientRect();

        this._element.addEventListener(<any>eventsMap.start, this._onTouchStart);
    }

    addListener(type: GestureType, handler: GestureHandler): void {
        const handlers = this._handlers[type] || (this._handlers[type] = []);
        handlers.push(handler);
    }

    removeListener(type: GestureType, handler: GestureHandler): void {
        const handlers = this._handlers[type];
        if (handlers) {
            const handlerIndex = handlers.indexOf(handler);
            if (handlerIndex > 0) {
                handlers.splice(handlerIndex, 1);
            }
        }
    }

    private _onTouchStart = (e: NativeEvent) => {
        if (this._currentGesture || !isValidEvent(e)) {
            return;
        }

        this._currentGesture = "tap";
        window.addEventListener(<any>eventsMap.progress, this._onTouchMove);
        window.addEventListener(<any>eventsMap.end, this._onTouchEnd);

        this._initialXY = getEventXY(e);
        this._elementRect = this._element.getBoundingClientRect();
    };

    private _onTouchMove = (e: NativeEvent) => {
        if (!isValidEvent(e)) {
            return;
        }

        if (this._currentGesture === "pan") {
            this._fire("pan", "indefinite", e);
            return;
        }

        const eventXY = getEventXY(e);
        if (Math.abs(eventXY[0] - this._initialXY[0]) >= gestureTapTolerance ||
            Math.abs(eventXY[1] - this._initialXY[1]) >= gestureTapTolerance) {
            this._currentGesture = "pan";
            this._fire("pan", "start", e);
        }
    };

    private _onTouchEnd = (e: NativeEvent) => {
        if (!this._currentGesture || !isValidEvent(e)) {
            return;
        }

        this._fire(this._currentGesture, this._currentGesture === "pan" ? "end" : "indefinite", e);

        this._currentGesture = undefined;
        window.removeEventListener(<any>eventsMap.progress, this._onTouchMove);
        window.removeEventListener(<any>eventsMap.end, this._onTouchEnd);
    };

    private _fire(type: GestureType, state: GestureState, e: NativeEvent): void {
        const handlers = this._handlers[type];
        if (!handlers) {
            return;
        }

        const eventXY = getEventXY(e);
        const x = Math.max(Math.min(eventXY[0] - this._elementRect.left, this._elementRect.width), 0);
        const y = Math.max(Math.min(eventXY[1] - this._elementRect.top, this._elementRect.height), 0);

        gestureEvent.type = type;
        gestureEvent.state = state;
        gestureEvent.x = x;
        gestureEvent.y = y;
        gestureEvent.elementWidth = this._elementRect.width;
        gestureEvent.elementHeight = this._elementRect.height;

        for (let i = 0; i < handlers.length; i++) {
            const handler = handlers[i];
            if (!handler(gestureEvent)) {
                e.preventDefault();
                e.stopPropagation();
            }
        }
    }
}
