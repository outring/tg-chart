import {applyDensity, create2DCanvas, createElement} from "./dom.js";
import {deepExtend, hasProperties} from "./object.js";
import {Chart, IChartColumn} from "./chart.js";
import {ChartView} from "./chartView.js";
import {GestureRecognizer, IGestureEvent} from "./gestureRecognizer.js";
import {cancelTransition, debounce, startTransition} from "./dynamics.js";
import {IChartColumnToggleable} from "./chartSwitch.js";

export interface IChartNavigatorStyle {
    paddingH: number;
    paddingV: number;
    transitionDuration: number;
    line: IChartNavigatorLineStyle;
    selection: IChartNavigatorSelectionStyle;
    selectionFrame: IChartNavigatorSelectionFrameStyle;
}

interface IChartNavigatorLineStyle {
    width: number;
    color: string;
}

interface IChartNavigatorSelectionStyle {
    color: string;
}

interface IChartNavigatorSelectionFrameStyle {
    color: string;
    widthH: number;
    widthV: number;
}

type PanMode = "area" | "from" | "to" | "indefinite";

const panTolerance = 9;

const defaultStyle: Readonly<IChartNavigatorStyle> = {
    paddingH: 0,
    paddingV: 1,
    transitionDuration: 100,
    line: {
        width: 1,
        color: "#666666"
    },
    selection: {
        color: "rgba(196, 196, 196, 0.5)"
    },
    selectionFrame: {
        color: "rgba(196, 196, 255, 0.75)",
        widthH: 1,
        widthV: 4
    }
};

export class ChartNavigator implements IChartColumnToggleable {

    private readonly _container = createElement("div", {className: "chartNavigator"});

    private _style: Readonly<IChartNavigatorStyle> = <any>{};
    private readonly _canvas: HTMLCanvasElement;
    private readonly _context: CanvasRenderingContext2D;

    private _chart = Chart.empty;

    private _view: ChartView<any> | undefined;

    private _width = 0;
    private _height = 0;

    private _maxY = 0;

    private _transitioningColumnsY: { [name: string]: boolean | undefined } = {};
    private _invisibleColumnsY: { [name: string]: boolean | undefined } = {};

    private _selectionFrom = 0.75;
    private _selectionTo = 1;

    private _panMode: PanMode = "indefinite";
    private _panDiffFrom = 0;
    private _panDiffTo = 0;

    private _isDirty = false;

    private _transitionStateY = 0;
    private _transitionIdY = -1;

    private get _columnsY(): ReadonlyArray<IChartColumn<number>> {
        return this._chart.columnsY;
    }

    private get _top(): number {
        return 0;
    }

    private get _right(): any {
        return this._width;
    }

    private get _bottom(): any {
        return this._height;
    }

    private get _left(): number {
        return 0;
    }

    private get _chartWidth(): number {
        return this._chartRight - this._chartLeft;
    }

    private get _chartHeight(): number {
        return this._chartBottom - this._chartTop;
    }

    private get _chartTop(): number {
        return this._top + this._style.paddingV + this._style.selectionFrame.widthH;
    }

    private get _chartRight(): any {
        return this._right - this._style.paddingH;
    }

    private get _chartBottom(): any {
        return this._bottom - this._style.paddingV - this._style.selectionFrame.widthH;
    }

    private get _chartLeft(): number {
        return this._left + this._style.paddingH;
    }

    constructor(style?: Partial<IChartNavigatorStyle>) {
        this.setStyle(style || {});

        [this._canvas, this._context] = create2DCanvas(this._container);

        const gestureRecognizer = new GestureRecognizer(this._container);
        gestureRecognizer.addListener("pan", this._onPan);
    }

    setChart(chart: Chart<any>): void {
        this._chart = chart;

        this._maxY = this._computeMaxY();

        this._requestRedraw();
    }

    setStyle(style: Partial<IChartNavigatorStyle>): void {
        this._style = applyDensity(deepExtend({}, defaultStyle, style));

        this._transitionY.interval = this._style.transitionDuration;

        this._requestRedraw();
    }

    appendTo(element: HTMLElement): void {
        element.appendChild(this._container);

        this._maxY = this._computeMaxY();

        window.removeEventListener("resize", this._updateSize);
        window.addEventListener("resize", this._updateSize);
        this._updateSize();

        this._requestRedraw();
    }

    addView(view: ChartView<any>): void {
        this._view = view;
        this._view.setVisibleArea(this._selectionFrom, this._selectionTo, true);
    }

    toggleColumn(name: string, show: boolean): void {
        this._transitioningColumnsY[name] = show;

        this._transitionY();
    }

    private _updateSize = () => {
        const newWidth = applyDensity(this._container.offsetWidth);
        const newHeight = applyDensity(this._container.offsetHeight);

        if (newWidth <= 0 || newHeight <= 0) {
            throw new Error("Container is invisible");
        }

        if (this._width !== newWidth) {
            this._canvas.width = newWidth;
            this._width = newWidth;
            this._requestRedraw();
        }

        if (this._height !== newHeight) {
            this._canvas.height = newHeight;
            this._height = newHeight;
            this._requestRedraw();
        }
    };

    private _transitionY = debounce(() => {
        const isTransitioning = this._transitionIdY !== -1;

        if (isTransitioning) {
            return;
        }

        const hasTransitioningColumns = hasProperties(this._transitioningColumnsY);
        if (hasTransitioningColumns) {
            this._initializeColumnsTransition();
        }

        const newMaxY = this._computeMaxY();
        if (newMaxY === this._maxY && !hasTransitioningColumns) {
            return;
        }

        const differenceY = newMaxY - this._maxY;
        cancelTransition(this._transitionIdY);
        this._transitionIdY = startTransition(0, 1, this._style.transitionDuration, y => {
            this._maxY = newMaxY - differenceY * (1 - y);
            this._transitionStateY = y;
            if (y === 1) {
                this._transitionIdY = -1;
                this._finalizeColumnsTransition();
            }
            this._requestRedraw();
        });
    });

    private _initializeColumnsTransition(): void {
        for (const columnName in this._transitioningColumnsY) {
            if (!this._transitioningColumnsY.hasOwnProperty(columnName)) {
                continue;
            }

            if (this._transitioningColumnsY[columnName]) {
                delete this._invisibleColumnsY[columnName];
            }
        }
    }

    private _finalizeColumnsTransition(): void {
        for (const columnName in this._transitioningColumnsY) {
            if (!this._transitioningColumnsY.hasOwnProperty(columnName)) {
                continue;
            }

            if (this._transitioningColumnsY[columnName]) {
                delete this._transitioningColumnsY[columnName];
            }
            else {
                this._invisibleColumnsY[columnName] = true;
            }
        }
    }

    private _computeMaxY(): number {
        let result = Number.MIN_VALUE;

        for (let i = 0; i < this._columnsY.length; i++) {
            const column = this._columnsY[i];

            if (this._invisibleColumnsY[column.name] || this._transitioningColumnsY[column.name] === false) {
                continue;
            }

            let value = this._chart.getMaxValue(i, 0, column.values.length - 1);
            if (value > result) {
                result = value;
            }
        }

        return result;
    }

    private _updateView(from: number, to: number): boolean {
        return this._view ? this._view.setVisibleArea(from, to) : true;
    }

    private _onPan = (e: IGestureEvent) => {
        const relativeX = e.x / e.elementWidth;
        const relativeTolerance = (this._style.selectionFrame.widthV + panTolerance) / e.elementWidth;

        if (e.state === "start") {
            this._panDiffFrom = this._selectionFrom - relativeX;
            this._panDiffTo = this._selectionTo - relativeX;

            const fromDifference = relativeX - this._selectionFrom;
            const toDifference = this._selectionTo - relativeX;

            if (fromDifference > relativeTolerance && toDifference > relativeTolerance) {
                this._panMode = "area";
            }
            else if (Math.abs(fromDifference) <= relativeTolerance) {
                this._panMode = "from";
            }
            else if (Math.abs(toDifference) <= relativeTolerance) {
                this._panMode = "to";
            }
            else {
                this._panMode = "indefinite";
            }
        }

        if (this._panMode === "indefinite") {
            return true;
        }

        const newFrom = Math.max(relativeX + this._panDiffFrom, 0);
        const newTo = Math.min(relativeX + this._panDiffTo, 1);
        const isMinimalFrom = newFrom === this._selectionFrom && newFrom === 0;
        const isMinimalTo = newTo === this._selectionTo && newTo === 1;

        switch (this._panMode) {
            case "area":
                if (isMinimalFrom || isMinimalTo) {
                    return true;
                }

                if (!this._updateView(newFrom, newTo)) {
                    return true;
                }

                this._selectionFrom = newFrom;
                this._selectionTo = newTo;
                break;
            case "from":
                if (isMinimalFrom || newFrom >= this._selectionTo) {
                    return true;
                }

                if (!this._updateView(newFrom, this._selectionTo)) {
                    return true;
                }

                this._selectionFrom = newFrom;
                break;
            case "to":
                if (isMinimalTo || newTo <= this._selectionFrom) {
                    return true;
                }

                if (!this._updateView(this._selectionFrom, newTo)) {
                    return true;
                }

                this._selectionTo = newTo;
                break;
        }

        this._requestRedraw();

        return false;
    };

    private _requestRedraw(): void {
        if (this._isDirty) {
            return;
        }

        this._isDirty = true;
        requestAnimationFrame(this._draw);
    }

    private _draw = () => {
        if (!this._isDirty) {
            return;
        }

        this._isDirty = false;

        this._context.clearRect(0, 0, this._width, this._height);

        this._drawSelectionFrame();
        this._drawLines();
        this._drawSelectionOverlay();
    };

    private _drawLines(): void {
        const style = this._style;
        const ctx = this._context;

        const isTransitioning = this._transitionIdY !== -1;

        ctx.lineJoin = "round";
        ctx.lineWidth = style.line.width;

        for (let i = 0; i < this._columnsY.length; i++) {
            const column = this._columnsY[i];

            ctx.strokeStyle = column.color || style.line.color;

            if (this._invisibleColumnsY[column.name]) {
                continue;
            }

            ctx.strokeStyle = column.color || style.line.color;

            const columnTransitionDirection = this._transitioningColumnsY[column.name];
            if (columnTransitionDirection === undefined) {
                ctx.globalAlpha = 1;
            }
            else if (isTransitioning) {
                ctx.globalAlpha = columnTransitionDirection ? this._transitionStateY : 1 - this._transitionStateY;
            }

            this._drawLine(column.values);
        }
    }

    private _drawSelectionFrame(): void {
        const style = this._style;
        const ctx = this._context;

        const selectionFromEndX = this._width * this._selectionFrom;
        const selectionToStartX = this._width * this._selectionTo;

        const selectionFrameFromVX = selectionFromEndX + style.selectionFrame.widthV / 2;
        const selectionFrameToVX = selectionToStartX - style.selectionFrame.widthV / 2;

        const selectionFrameFromHX = selectionFromEndX + style.selectionFrame.widthV;
        const selectionFrameToHX = selectionToStartX - style.selectionFrame.widthV;

        ctx.strokeStyle = style.selectionFrame.color;
        ctx.globalAlpha = 1;

        ctx.lineWidth = style.selectionFrame.widthV;
        ctx.beginPath();
        ctx.moveTo(selectionFrameFromVX, this._top);
        ctx.lineTo(selectionFrameFromVX, this._bottom);
        ctx.moveTo(selectionFrameToVX, this._top);
        ctx.lineTo(selectionFrameToVX, this._bottom);
        ctx.stroke();

        ctx.lineWidth = style.selectionFrame.widthH;
        ctx.beginPath();
        ctx.moveTo(selectionFrameFromHX, this._top);
        ctx.lineTo(selectionFrameToHX, this._top);
        ctx.moveTo(selectionFrameFromHX, this._bottom);
        ctx.lineTo(selectionFrameToHX, this._bottom);
        ctx.stroke();
    }

    private _drawSelectionOverlay(): void {
        const style = this._style;
        const ctx = this._context;

        const selectionFromStartX = this._left;
        const selectionFromEndX = this._width * this._selectionFrom;

        const selectionToStartX = this._width * this._selectionTo;
        const selectionToEndX = this._right;

        ctx.fillStyle = style.selection.color;
        ctx.globalAlpha = 1;

        ctx.fillRect(selectionFromStartX, this._top, selectionFromEndX, this._bottom);
        ctx.fillRect(selectionToStartX, this._top, selectionToEndX, this._bottom);
    };

    private _drawLine(values: ReadonlyArray<number>): void {
        const ctx = this._context;

        ctx.beginPath();

        for (let i = 0; i < values.length; i++) {
            const value = values[i];
            const valueX = this._chartLeft + this._chartWidth * i / (values.length - 1);
            const valueY = this._chartTop + this._chartHeight - this._chartHeight * (value / this._maxY);

            if (i === 0) {
                ctx.moveTo(valueX, valueY);
            }
            else {
                ctx.lineTo(valueX, valueY);
            }
        }

        ctx.stroke();
    }
}
