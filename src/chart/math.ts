/**
 * Computes closest smaller power of 2
 * @param x
 * @param getClosestPower
 */
export function closestPow2(x: number, getClosestPower: (x: number) => number): number {
    if (x === 0) {
        return 1;
    }

    if (x < 0) {
        throw new Error("Expected positive number");
    }

    const power = getClosestPower(Math.log(x) * Math.LOG2E);
    if (power < 0) {
        return 1;
    }

    return Math.pow(2, power);
}

/**
 * Provides max value of index range
 */
export class RangeMaxQuery {

    private readonly _valuesCount: number;
    private readonly _maxValues: number[];

    private _lastFromIndex = -1;
    private _lastToIndex = -1;
    private _lastResult = Number.MIN_VALUE;

    constructor(values: ReadonlyArray<number>) {
        this._valuesCount = values.length;

        const storageSize = 2 * closestPow2(this._valuesCount, Math.ceil) - 1;
        this._maxValues = new Array(storageSize);

        this._computeValue(values, 0, 0, this._valuesCount - 1);
    }

    getValue(fromIndex: number, toIndex: number): number {
        // We're querying it for the same range a lot so optimizing a bit
        if (this._lastFromIndex === fromIndex && this._lastToIndex === toIndex) {
            return this._lastResult;
        }

        const result = this._getValue(0, 0, this._valuesCount - 1, fromIndex, toIndex);

        this._lastFromIndex = fromIndex;
        this._lastToIndex = toIndex;
        this._lastResult = result;

        return result;
    }

    private _getValue(storedIndex: number, storedFromIndex: number, storedToIndex: number, fromIndex: number, toIndex: number): number {
        if (storedFromIndex > toIndex || storedToIndex < fromIndex) {
            return Number.MIN_VALUE;
        }

        if (storedFromIndex >= fromIndex && storedToIndex <= toIndex) {
            return this._maxValues[storedIndex];
        }

        const storedMiddleIndex = Math.floor((storedFromIndex + storedToIndex) / 2);
        return Math.max(
            this._getValue(storedIndex * 2 + 1, storedFromIndex, storedMiddleIndex, fromIndex, toIndex),
            this._getValue(storedIndex * 2 + 2, storedMiddleIndex + 1, storedToIndex, fromIndex, toIndex));
    }

    private _computeValue(values: ReadonlyArray<number>, storedIndex: number, fromIndex: number, toIndex: number): number {
        let value: number;

        if (fromIndex === toIndex) {
            value = values[fromIndex];
        }
        else {
            const middleIndex = Math.floor((fromIndex + toIndex) / 2);
            value = Math.max(
                this._computeValue(values, storedIndex * 2 + 1, fromIndex, middleIndex),
                this._computeValue(values, storedIndex * 2 + 2, middleIndex + 1, toIndex));
        }

        this._maxValues[storedIndex] = value;

        return value;
    }

}
