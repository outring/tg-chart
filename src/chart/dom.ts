import {deepExtend} from "./object.js";

export function createElement<T extends keyof HTMLElementTagNameMap>(tagName: T, props?: { [prop: string]: any }, childElements?: (HTMLElement | string)[]): HTMLElementTagNameMap[T] {
    const result = document.createElement<T>(tagName);

    if (props) {
        deepExtend(result, props);
    }

    if (childElements) {
        for (let i = 0; i < childElements.length; i++) {
            const child = childElements[i];
            if (child instanceof HTMLElement) {
                result.appendChild(child);
            }
            else {
                result.appendChild(document.createTextNode(child));
            }
        }
    }

    return result;
}

export function create2DCanvas(container: HTMLElement): [HTMLCanvasElement, CanvasRenderingContext2D] {
    const canvas = createElement("canvas");
    deepExtend(canvas.style, {
        position: "absolute",
        width: "100%",
        height: "100%"
    });

    const position = getComputedStyle(container).position;
    if (!position || position === "static") {
        container.style.position = "relative";
    }
    container.appendChild(canvas);

    const context = canvas.getContext("2d");
    if (!context) {
        throw new Error("Couldn't create a 2D rendering context for the canvas");
    }

    return [canvas, context];
}

/**
 * Applies `devicePixelRatio` to number values (even nested)
 * @param entity
 */
export function applyDensity<T>(entity: T): T {
    if (typeof entity === "number") {
        return <any>(entity * (window.devicePixelRatio || 1));
    }

    if (typeof entity === "object") {
        const result: any = {};
        for (const property in entity) {
            if (entity.hasOwnProperty(property)) {
                result[property] = applyDensity(entity[property]);
            }
        }
        return result;
    }

    if (Array.isArray(entity)) {
        return <any>entity.map(applyDensity);
    }

    return entity;
}

export function addClassName(element: HTMLElement, className: string): void {
    if (element.classList) {
        element.classList.add(className);
        return;
    }

    const classes = element.className.split(/\s+/).filter(x => x);
    if (classes.indexOf(className) === -1) {
        classes.push(className);
    }

    element.className = classes.join(" ");
}

export function removeClassName(element: HTMLElement, className: string): void {
    if (element.classList) {
        element.classList.remove(className);
        return;
    }

    const classes = element.className.split(/\s+/).filter(x => x);
    const classIndex = classes.indexOf(className);
    if (classIndex !== -1) {
        classes.splice(classIndex, 1);
    }

    element.className = classes.join(" ");
}

export function toggleClassName(element: HTMLElement, className: string, add: boolean): void {
    if (add) {
        addClassName(element, className);
    }
    else {
        removeClassName(element, className);
    }
}
