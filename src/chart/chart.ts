import {RangeMaxQuery} from "./math.js";

type ChartColumnType = "x" | "line";

export interface IChartData<TX> {
    readonly types: { readonly [columnKey: string]: ChartColumnType };
    readonly columns: [string, ...(TX | number)[]][];
    readonly names: { readonly [columnKey: string]: string };
    readonly colors: { readonly [columnKey: string]: string };
}

export interface IChartColumn<T> {
    readonly name: string;
    readonly values: ReadonlyArray<T>;
    readonly label: string;
    readonly color: string;
}

const COLUMN_TYPE_X: ChartColumnType = "x";
const COLUMN_TYPE_Y: ChartColumnType = "line";
const COLUMN_DATA_INDEX = 1;

export class Chart<TX> {

    static readonly empty = new Chart<any>({values: [], label: "", color: "", name: ""}, []);

    readonly columnX: IChartColumn<TX>;
    readonly columnsY: IChartColumn<number>[];
    private readonly _maxValuesY: RangeMaxQuery[];

    private constructor(columnX: IChartColumn<TX>, columnsY: IChartColumn<number>[]) {
        this.columnX = columnX;
        this.columnsY = columnsY;

        this._maxValuesY = this.columnsY.map(x => new RangeMaxQuery(x.values));
    }

    static fromData<TX>(chart: IChartData<TX>): Chart<TX>
    static fromData<TXIn, TXOut>(chart: IChartData<TXIn>, convertX: (value: TXIn) => TXOut): Chart<TXOut>
    static fromData(chart: IChartData<any>, convertX?: (value: any) => any): Chart<any> {
        const columnNameX = Object.keys(chart.types).filter(x => chart.types[x] === COLUMN_TYPE_X)[0];
        if (!columnNameX) {
            throw new Error(`Couldn't find a column type for ${COLUMN_TYPE_X}`);
        }

        const columnXData = chart.columns.filter(x => x[0] === columnNameX)[0];
        if (!columnXData) {
            throw new Error(`Couldn't find a column for ${COLUMN_TYPE_X}`);
        }

        const valuesX = <any[]>columnXData.slice(COLUMN_DATA_INDEX);
        const columnX = {
            name: columnNameX,
            values: convertX ? valuesX.map(convertX) : valuesX,
            label: "",
            color: ""
        };

        const columnsY = chart.columns
            .filter(x => chart.types[x[0]] === COLUMN_TYPE_Y)
            .map(x => ({
                name: x[0],
                values: <number[]>x.slice(COLUMN_DATA_INDEX),
                label: chart.names[x[0]],
                color: chart.colors[x[0]]
            }));

        return new Chart(columnX, columnsY);
    }

    getMaxValue(columnIndex: number, fromIndex: number, toIndex: number): number {
        return this._maxValuesY[columnIndex].getValue(fromIndex, toIndex);
    }
}
