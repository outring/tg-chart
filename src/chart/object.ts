export function deepExtend<TD extends {}, TS1 extends {}, TS2 extends {}>(destination: TD, source1: TS1, source2?: TS2): TD & TS1 & TS2
export function deepExtend(destination: any, ...sources: any[]): any {
    for (let i = 0; i < sources.length; i++) {
        const source = sources[i];
        for (const property in source) {
            if (source.hasOwnProperty(property)) {
                destination[property] = typeof source[property] === "object"
                    ? deepExtend(destination[property] || {}, source[property])
                    : destination[property] = source[property];
            }
        }
    }

    return destination;
}

export function hasProperties(object: {}): boolean {
    for (const property in object) {
        if (object.hasOwnProperty(property)) {
            return true;
        }
    }

    return false;
}

export function deleteProperty(object: {}, property: string): void {
    for (const key in object) {
        if (object.hasOwnProperty(key) && key === property) {
            delete (<any>object)[key];
            return;
        }
    }
}
