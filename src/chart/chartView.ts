import {deepExtend, hasProperties} from "./object.js";
import {Chart, IChartColumn} from "./chart.js";
import {closestPow2} from "./math.js";
import {cancelTransition, debounce, startTransition} from "./dynamics.js";
import {applyDensity, create2DCanvas, createElement} from "./dom.js";
import {IChartColumnToggleable} from "./chartSwitch";

interface IChartOptions<TX> {
    gridDensityX: number;
    gridDensityY: number;
    formatLabelX?(value: TX): string;
    formatLabelY?(value: number, columnName?: string): string;
}

export interface IChartStyle {
    grid: Partial<IChartGridStyle>;
    gridX: Partial<IChartGridStyle>;
    gridY: Partial<IChartGridStyle>;
    gridLabel: Partial<IChartGridLabelStyle>;
    gridLabelX: Partial<IChartGridLabelStyle>;
    gridLabelY: Partial<IChartGridLabelStyle>;
    line: Partial<IChartLineStyle>;
}

interface IInternalChartStyle {
    grid: IChartGridStyle;
    gridX: IChartGridStyle;
    gridY: IChartGridStyle;
    gridLabel: IChartGridLabelStyle;
    gridLabelX: IChartGridLabelStyle;
    gridLabelY: IChartGridLabelStyle;
    line: IChartLineStyle;
}

interface IChartGridStyle {
    width: number;
    color: string;
    transitionDuration: number;
}

interface IFontStyle {
    fontFamily: string;
    fontSize: number;
}

interface IChartGridLabelStyle extends IFontStyle {
    padding: number;
    color: string;
}

interface IChartLineStyle {
    width: number;
    color: string;
}

interface IChartGridState {
    from: number;
    to: number;
    step: number;
}

const measuresCountMin = 5;
const measuresCountRatio = 0.25;

const defaultOptions: Readonly<IChartOptions<any>> = {
    gridDensityX: 0.5,
    gridDensityY: 0.2
};

const defaultStyle: Readonly<IInternalChartStyle> = {
    grid: {
        width: 1,
        color: "#cccccc",
        transitionDuration: 100,
    },
    gridX: <any>{},
    gridY: <any>{},
    gridLabel: {
        padding: 5,
        fontFamily: "sans-serif",
        fontSize: 12,
        color: "#cccccc",
    },
    gridLabelX: <any>{},
    gridLabelY: <any>{},
    line: {
        width: 2,
        color: "#666666"
    }
};

function getCanvasFont(style: IFontStyle): string {
    return `${style.fontSize}px ${style.fontFamily}`;
}

export class ChartView<TX> implements IChartColumnToggleable {

    private readonly _container: HTMLElement = createElement("div", {className: "chartView"});

    private readonly _options: Readonly<IChartOptions<TX>>;
    private _style: Required<Readonly<IInternalChartStyle>> = <any>{};
    private readonly _gridCanvas: HTMLCanvasElement;
    private readonly _gridContext: CanvasRenderingContext2D;
    private readonly _linesCanvas: HTMLCanvasElement;
    private readonly _linesContext: CanvasRenderingContext2D;

    private readonly _labelCacheX: { [key: string]: string } = {};
    private readonly _labelCacheY: { [key: string]: string } = {};

    private _chart: Chart<TX> = Chart.empty;

    private readonly _minY: number = 0;
    private _maxY = 0;

    private _transitioningColumnsY: { [name: string]: boolean | undefined } = {};
    private _invisibleColumnsY: { [name: string]: boolean | undefined } = {};

    private _visibleAreaFrom = 0;
    private _visibleAreaTo = 1;

    private _width = 0;
    private _height = 0;
    private _gridLabelWidthX = 0;

    private _isGridDirty = false;
    private _isLinesDirty = false;

    private readonly _gridStateX: IChartGridState = {from: 0, to: 0, step: -1};
    private readonly _alternativeGridStateX: IChartGridState = {from: 0, to: 0, step: -1};
    private readonly _gridStateY: IChartGridState = {from: 0, to: 0, step: 1};
    private readonly _alternativeGridStateY: IChartGridState = {from: 0, to: 0, step: 1};

    private _transitionStateX = 0;
    private _transitionStateY = 0;
    private _transitionIdX = -1;
    private _transitionIdY = -1;

    private get _columnX(): IChartColumn<TX> {
        return this._chart.columnX;
    }

    private get _columnsY(): ReadonlyArray<IChartColumn<number>> {
        return this._chart.columnsY;
    }

    private get _top(): number {
        return 0;
    }

    private get _right(): any {
        return this._width;
    }

    private get _bottom(): any {
        return this._height;
    }

    private get _left(): number {
        return 0;
    }

    private get _chartWidth(): number {
        return this._chartRight - this._chartLeft;
    }

    private get _chartHeight(): number {
        return this._chartBottom - this._chartTop;
    }

    private get _chartRight(): any {
        return this._right;
    }

    private get _chartBottom(): number {
        return this._bottom - this._style.gridLabelX.fontSize - this._style.gridLabelX.padding - this._style.gridX.width;
    }

    private get _chartLeft(): number {
        return this._left + this._style.gridY.width;
    }

    private get _lastValueIndex(): number {
        return this._columnX.values.length - 1;
    }

    private get _visibleInitialValueIndexPrecise(): number {
        return this._lastValueIndex * this._visibleAreaFrom;
    }

    private get _visibleFinalValueIndexPrecise(): number {
        return this._lastValueIndex * this._visibleAreaTo;
    }

    private get _visibleInitialValueIndex(): number {
        return Math.floor(this._visibleInitialValueIndexPrecise);
    }

    private get _visibleFinalValueIndex(): number {
        return Math.ceil(this._visibleFinalValueIndexPrecise);
    }

    private get _chartTop(): number {
        return this._top + this._style.line.width / 2;
    }

    private get _visibleLabelsCountX(): number {
        const visibleLabelsArea = this._options.gridDensityX * this._chartWidth;
        return Math.ceil(visibleLabelsArea / this._gridLabelWidthX);
    }

    private get _visibleLabelsCountY(): number {
        const visibleLabelsArea = this._options.gridDensityY * this._chartHeight;
        return Math.floor(visibleLabelsArea / this._style.gridLabelY.fontSize);
    }

    private get _visibleValuesCount(): number {
        return Math.ceil(this._visibleFinalValueIndexPrecise - this._visibleInitialValueIndexPrecise);
    }

    constructor(options?: Partial<IChartOptions<TX>>, style?: Partial<IChartStyle>) {
        this._options = deepExtend({}, defaultOptions, options);

        this.setStyle(style || {});

        [this._gridCanvas, this._gridContext] = create2DCanvas(this._container);
        [this._linesCanvas, this._linesContext] = create2DCanvas(this._container);
    }

    setChart(chart: Chart<TX>): void {
        this._chart = chart;

        this._measure();

        this._computeX(true);
        this._computeY(true);

        this._requestRedraw();
    }

    setVisibleArea(from: number, to: number, immediate = false): boolean {
        if ((to - from) * this._columnX.values.length < 2) {
            return false;
        }

        this._visibleAreaFrom = from;
        this._visibleAreaTo = to;

        this._computeX(immediate);
        this._computeY(immediate);

        this._requestRedraw();

        return true;
    }

    appendTo(element: HTMLElement): void {
        element.appendChild(this._container);

        this._measure();

        this._computeX(true);
        this._computeY(true);

        window.removeEventListener("resize", this._updateSize);
        window.addEventListener("resize", this._updateSize);
        this._updateSize();

        this._requestRedraw();
    }

    private _getChartXByValueIndex(index: number): number {
        const indexProportion = (index - this._visibleInitialValueIndexPrecise) / (this._visibleFinalValueIndexPrecise - this._visibleInitialValueIndexPrecise);
        return this._chartLeft + this._chartWidth * indexProportion;
    }

    private _getChartYByValue(value: number): number {
        const valueProportion = value / this._maxY;
        return this._chartBottom - this._chartHeight * valueProportion;
    }

    private _measure(): void {
        this._gridLabelWidthX = this._measureLabelsX();
    }

    private _measureLabelsX(): number {
        const style = this._style;
        const ctx = this._gridContext;

        const dataValuesCount = this._columnX.values.length;
        const measuresCount = Math.max(dataValuesCount * measuresCountRatio, measuresCountMin);
        const incrementSize = Math.floor(dataValuesCount / measuresCount) || 1;

        ctx.font = getCanvasFont(style.gridLabelX);

        let result = Number.MIN_VALUE;

        for (let i = 0; i < this._columnX.values.length; i += incrementSize) {
            const label = this._getLabelX(this._columnX.values[i]);
            const labelWidth = ctx.measureText(label).width;
            if (labelWidth > result) {
                result = labelWidth;
            }
        }

        return result;
    }

    setStyle(style: Partial<IChartStyle>): void {
        const newStyle = <IInternalChartStyle>applyDensity(deepExtend({}, defaultStyle, style));
        newStyle.gridX = deepExtend({}, newStyle.grid, newStyle.gridX);
        newStyle.gridY = deepExtend({}, newStyle.grid, newStyle.gridY);
        newStyle.gridLabelX = deepExtend({}, newStyle.gridLabel, newStyle.gridLabelX);
        newStyle.gridLabelY = deepExtend({}, newStyle.gridLabel, newStyle.gridLabelY);
        this._style = newStyle;

        this._transitionY.interval = newStyle.gridY.transitionDuration;

        this._requestRedraw();
    }

    private _computeX(immediate: boolean): void {
        if (immediate) {
            cancelTransition(this._transitionIdX);
            this._transitionIdX = -1;
            this._computeGridStateX(this._gridStateX);
        }
        else {
            this._transitionX();
        }
    }

    private _computeGridStateX(state: IChartGridState): void {
        const step = this._computeGridStepX();
        this._computeGridStateForStepX(state, step);
    }

    private _computeGridStepX(): number {
        const visibleDivisionsCount = this._visibleValuesCount / (this._visibleLabelsCountX - 1);
        return -closestPow2(visibleDivisionsCount, Math.floor) || 1;
    }

    private _computeGridStateForStepX(state: IChartGridState, step: number): void {
        state.step = step;

        // We don't want the rightmost label to be ever obscured so calculating the stable(!) offset
        // for the worst case scenario (full zoom-out)
        // When the `step` is less than the safe offset it's better use the step, since it's only 2 times smaller
        // which also guarantees non-obscured display
        const initialIndex = this._lastValueIndex * this._gridLabelWidthX * 0.5 / this._chartWidth;
        const labelDisplayAdjustment = Math.min(closestPow2(initialIndex, Math.floor), -state.step);

        // We want x-axis labels to be stable during scrolling and resizing
        const panningAdjustment = (this._visibleFinalValueIndex - this._lastValueIndex) % state.step;

        state.from = this._visibleFinalValueIndex - panningAdjustment - labelDisplayAdjustment;
        state.to = Math.max(this._visibleInitialValueIndex + state.step, 0);
    }

    private _computeY(immediate: boolean): void {
        if (immediate) {
            cancelTransition(this._transitionIdY);
            this._transitionIdY = -1;
            this._maxY = this._computeMaxY();
            this._computeGridStateY(this._gridStateY);
        }
        else {
            this._transitionY();
        }
    }

    private _computeGridStateY(state: IChartGridState): void {
        const step = this._computeGridStepY(this._maxY);
        this._computeGridStateForStepY(state, step);
    }

    toggleColumn(name: string, show: boolean): void {
        this._transitioningColumnsY[name] = show;

        this._computeY(false);
    }

    private _computeGridStateForStepY(state: IChartGridState, step: number): void {
        state.step = step;
        state.from = this._minY;
        state.to = Math.floor(this._maxY / step) * step;
    }

    private _computeGridStepY(maxY: number): number {
        const style = this._style;

        const maxValue = maxY - Math.floor(maxY * (style.gridLabelY.fontSize + style.gridLabelY.padding) / this._chartHeight);
        const visibleDivisionsCount = maxValue / (this._visibleLabelsCountY - 1);
        return Math.floor(visibleDivisionsCount) || 1;
    }

    private _transitionX(): void {
        const newStep = this._computeGridStepX();
        const isTransitioning = this._transitionIdX !== -1;

        // If we're not transitioning and don't need to, just behave normally
        if (!isTransitioning && this._gridStateX.step === newStep) {
            this._computeGridStateX(this._gridStateX);
            return;
        }

        const isAppearing = newStep > this._gridStateX.step;

        // If we're transitioning and the step is not changed keep the current transition
        const transitioningState = isAppearing ? this._alternativeGridStateX : this._gridStateX;
        if (isTransitioning && transitioningState.step === newStep) {
            return;
        }

        if (isAppearing) { // The new state has more divisions than the current, showing the new as an alternative
            this._computeGridStateX(this._alternativeGridStateX);
            this._computeGridStateForStepX(this._gridStateX, this._gridStateX.step);
        }
        else { // The new state has less divisions than the current, showing the current as an alternative
            this._computeGridStateForStepX(this._alternativeGridStateX, this._gridStateX.step);
            this._computeGridStateX(this._gridStateX);
        }

        cancelTransition(this._transitionIdX);
        this._transitionIdX = startTransition(0, 1, this._style.gridX.transitionDuration, x => {
            this._transitionStateX = isAppearing ? x : 1 - x;
            if (x === 1) {
                this._transitionIdX = -1;
                this._computeGridStateX(this._gridStateX);
            }
            else {
                this._computeGridStateForStepX(this._gridStateX, this._gridStateX.step);
                this._computeGridStateForStepX(this._alternativeGridStateX, this._alternativeGridStateX.step);
            }
            this._requestGridRedraw();
        });
    }

    private _transitionY = debounce(() => {
        const isTransitioning = this._transitionIdY !== -1;

        if (isTransitioning) {
            return;
        }

        const hasTransitioningColumns = hasProperties(this._transitioningColumnsY);
        if (hasTransitioningColumns) {
            this._initializeColumnsTransition();
        }

        const newMaxY = this._computeMaxY();
        if (newMaxY === this._maxY && !hasTransitioningColumns) {
            return;
        }

        const newStep = this._computeGridStepY(newMaxY);
        this._computeGridStateForStepY(this._alternativeGridStateY, newStep);

        const differenceY = newMaxY - this._maxY;
        cancelTransition(this._transitionIdY);
        this._transitionIdY = startTransition(0, 1, this._style.gridY.transitionDuration, y => {
            this._maxY = newMaxY - differenceY * (1 - y);
            this._transitionStateY = y;
            if (y === 1) {
                this._transitionIdY = -1;
                this._computeGridStateForStepY(this._gridStateY, this._alternativeGridStateY.step);
                this._finalizeColumnsTransition();
            }
            else {
                this._computeGridStateForStepY(this._gridStateY, this._gridStateY.step);
                this._computeGridStateForStepY(this._alternativeGridStateY, this._alternativeGridStateY.step);
            }
            this._requestRedraw();
        });
    });

    /**
     * Computes max Y value taking into account partially obscured
     * parts of the lines to ensure smooth scaling
     */
    private _computeMaxY(): number {
        let result = Number.MIN_VALUE;

        for (let i = 0; i < this._columnsY.length; i++) {
            const column = this._columnsY[i];

            if (this._invisibleColumnsY[column.name] || this._transitioningColumnsY[column.name] === false) {
                continue;
            }

            const values = column.values;

            const hasObscuredLeft = this._visibleInitialValueIndex > 0;
            const hasObscuredRight = this._visibleFinalValueIndex < values.length - 1;

            let value = this._chart.getMaxValue(i,
                hasObscuredLeft ? this._visibleInitialValueIndex + 1 : this._visibleInitialValueIndex,
                hasObscuredRight ? this._visibleFinalValueIndex - 1 : this._visibleFinalValueIndex);

            if (hasObscuredLeft) {
                const neighbourIndex = Math.ceil(this._visibleInitialValueIndexPrecise);
                const valuesDifference = values[this._visibleInitialValueIndex] - values[neighbourIndex];
                const differenceFraction = this._visibleInitialValueIndexPrecise - this._visibleInitialValueIndex;
                value = Math.max(value, values[this._visibleInitialValueIndex] - Math.ceil(valuesDifference * differenceFraction));
            }

            if (hasObscuredRight) {
                const neighbourIndex = Math.floor(this._visibleFinalValueIndexPrecise);
                const valuesDifference = values[this._visibleFinalValueIndex] - values[neighbourIndex];
                const differenceFraction = this._visibleFinalValueIndex - this._visibleFinalValueIndexPrecise;
                value = Math.max(value, values[this._visibleFinalValueIndex] - Math.ceil(valuesDifference * differenceFraction));
            }

            if (value > result) {
                result = value;
            }
        }

        return result;
    }

    private _initializeColumnsTransition(): void {
        for (const columnName in this._transitioningColumnsY) {
            if (!this._transitioningColumnsY.hasOwnProperty(columnName)) {
                continue;
            }

            if (this._transitioningColumnsY[columnName]) {
                delete this._invisibleColumnsY[columnName];
            }
        }
    }

    private _finalizeColumnsTransition(): void {
        for (const columnName in this._transitioningColumnsY) {
            if (!this._transitioningColumnsY.hasOwnProperty(columnName)) {
                continue;
            }

            if (this._transitioningColumnsY[columnName]) {
                delete this._transitioningColumnsY[columnName];
            }
            else {
                this._invisibleColumnsY[columnName] = true;
            }
        }
    }

    private _updateSize = () => {
        const newWidth = applyDensity(this._container.offsetWidth);
        const newHeight = applyDensity(this._container.offsetHeight);

        if (newWidth <= 0 || newHeight <= 0) {
            throw new Error("Container is invisible");
        }

        if (this._width !== newWidth) {
            this._gridCanvas.width = newWidth;
            this._linesCanvas.width = newWidth;
            this._width = newWidth;
            this._requestRedraw();
        }

        if (this._height !== newHeight) {
            this._gridCanvas.height = newHeight;
            this._linesCanvas.height = newHeight;
            this._height = newHeight;
            this._requestRedraw();
        }
    };

    private _requestRedraw(): void {
        if (this._isGridDirty && this._isLinesDirty) {
            return;
        }

        this._isGridDirty = true;
        this._isLinesDirty = true;
        requestAnimationFrame(this._draw);
    }

    private _requestGridRedraw(): void {
        if (this._isGridDirty) {
            return;
        }

        this._isGridDirty = true;
        requestAnimationFrame(this._drawGrid);
    }

    private _draw = () => {
        this._drawGrid();
        this._drawLines();
    };

    private _drawGrid = () => {
        if (!this._isGridDirty) {
            return;
        }

        this._isGridDirty = false;

        this._gridContext.clearRect(0, 0, this._width, this._height);

        if (this._columnX) {
            this._drawGridX();
        }

        if (this._columnsY.length > 0) {
            this._drawGridY();
        }
    };

    private _drawLines = () => {
        if (!this._isLinesDirty) {
            return;
        }

        this._isLinesDirty = false;

        const style = this._style;
        const ctx = this._linesContext;

        const isTransitioning = this._transitionIdY !== -1;

        ctx.clearRect(0, 0, this._width, this._height);

        ctx.lineJoin = "round";
        ctx.lineWidth = style.line.width;

        for (let i = 0; i < this._columnsY.length; i++) {
            const column = this._columnsY[i];

            if (this._invisibleColumnsY[column.name]) {
                continue;
            }

            ctx.strokeStyle = column.color || style.line.color;

            const columnTransitionDirection = this._transitioningColumnsY[column.name];
            if (columnTransitionDirection === undefined) {
                ctx.globalAlpha = 1;
            }
            else if (isTransitioning) {
                ctx.globalAlpha = columnTransitionDirection ? this._transitionStateY : 1 - this._transitionStateY;
            }

            this._drawLine(column.values);
        }
    };

    private _drawGridX(): void {
        const style = this._style;
        const ctx = this._gridContext;

        const isTransitioning = this._transitionIdX !== -1;
        const hasGridLines = style.gridX.width > 0;

        ctx.font = getCanvasFont(style.gridLabelX);
        ctx.fillStyle = style.gridLabelX.color;
        ctx.textAlign = "center";
        ctx.textBaseline = "top";

        ctx.lineWidth = style.gridX.width;
        ctx.strokeStyle = style.gridX.color;

        ctx.globalAlpha = 1;

        this._drawLabelsX(this._gridStateX);

        if (hasGridLines) {
            this._drawGridLinesX(this._gridStateX);
        }

        if (isTransitioning) {
            ctx.globalAlpha = this._transitionStateX;

            this._drawLabelsX(this._alternativeGridStateX);

            if (hasGridLines) {
                this._drawGridLinesX(this._alternativeGridStateX);
            }
        }
    }

    private _drawGridY(): void {
        const style = this._style;
        const ctx = this._gridContext;

        const isTransitioning = this._transitionIdY !== -1;
        const hasGridLines = style.gridY.width > 0;

        ctx.font = getCanvasFont(style.gridLabelY);
        ctx.fillStyle = style.gridLabelX.color;
        ctx.textAlign = "left";
        ctx.textBaseline = "bottom";

        ctx.strokeStyle = style.gridX.color;
        ctx.lineWidth = style.gridX.width;

        ctx.globalAlpha = isTransitioning ? (1 - this._transitionStateY) : 1;

        this._drawLabelsY(this._gridStateY);

        if (hasGridLines) {
            this._drawGridLinesY(this._gridStateY);
        }

        if (isTransitioning) {
            ctx.globalAlpha = this._transitionStateY;

            this._drawLabelsY(this._alternativeGridStateY);

            if (hasGridLines) {
                this._drawGridLinesY(this._alternativeGridStateY);
            }
        }
    }

    private _drawLabelsX(state: IChartGridState): void {
        for (let i = state.from; i >= state.to; i += state.step) {
            const label = this._getLabelX(this._columnX.values[i]);
            const labelX = this._getChartXByValueIndex(i);
            const labelY = this._chartBottom + this._style.gridLabelX.padding;

            this._gridContext.fillText(label, labelX, labelY);
        }
    }

    private _drawLabelsY(state: IChartGridState): void {
        for (let value = state.from; value <= state.to; value += state.step) {
            const label = this._getLabelY(value);
            const labelX = this._chartLeft;
            const labelY = this._getChartYByValue(value) - this._style.gridLabelY.padding;

            this._gridContext.fillText(label, labelX, labelY);
        }
    }

    private _drawGridLinesX(state: IChartGridState): void {
        const ctx = this._gridContext;

        for (let i = state.from; i >= state.to; i += state.step) {
            const gridX = this._getChartXByValueIndex(i) - this._style.gridX.width / 2;

            ctx.beginPath();
            ctx.moveTo(gridX, this._chartTop);
            ctx.lineTo(gridX, this._chartBottom);
            ctx.stroke();
        }
    }

    private _drawGridLinesY(state: IChartGridState): void {
        const ctx = this._gridContext;

        for (let value = state.from; value <= state.to; value += state.step) {
            const gridY = this._getChartYByValue(value) - this._style.gridY.width / 2;

            ctx.beginPath();
            ctx.moveTo(this._chartLeft, gridY);
            ctx.lineTo(this._chartRight, gridY);
            ctx.stroke();
        }
    }

    private _drawLine(values: ReadonlyArray<number>): void {
        const ctx = this._linesContext;

        ctx.beginPath();

        for (let i = this._visibleInitialValueIndex; i <= this._visibleFinalValueIndex; i++) {
            const value = values[i];
            const valueX = this._getChartXByValueIndex(i);
            const valueY = this._getChartYByValue(value);

            if (i === this._visibleInitialValueIndex) {
                ctx.moveTo(valueX, valueY);
            }
            else {
                ctx.lineTo(valueX, valueY);
            }
        }

        ctx.stroke();
    }

    private _getLabelX(value: TX): string {
        //Do not cache if we're not doing anything special
        const formatLabelX = this._options.formatLabelX;
        if (!formatLabelX) {
            return value.toString();
        }

        const cacheKey = value.toString();
        return this._labelCacheX[cacheKey] ||
            (this._labelCacheX[cacheKey] = formatLabelX(value));
    }

    private _getLabelY(value: number, columnName?: string): string {
        //Do not cache if we're not doing anything special
        const formatLabelY = this._options.formatLabelY;
        if (!formatLabelY) {
            return value.toString();
        }

        const cacheKey = value.toString();
        return this._labelCacheY[cacheKey] ||
            (this._labelCacheY[cacheKey] = formatLabelY(value, columnName));
    }

}
