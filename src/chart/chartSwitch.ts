import {Chart, IChartColumn} from "./chart.js";
import {createElement, toggleClassName} from "./dom.js";

export interface IChartColumnToggleable {
    toggleColumn(name: string, show: boolean): void;
}

export class ChartSwitch {

    private readonly _container = createElement("div", {className: "chartSwitch"});

    private _chart = Chart.empty;

    private _views: IChartColumnToggleable[] = [];

    appendTo(element: HTMLElement): void {
        element.appendChild(this._container);
    }

    setChart(chart: Chart<any>): void {
        this._chart = chart;

        this._render();
    }

    addView(view: IChartColumnToggleable): void {
        this._views.push(view);
    }

    private _render(): void {
        for (let i = 0; i < this._container.children.length; i++) {
            const child = this._container.children[i];
            this._container.removeChild(child);
        }

        for (let i = 0; i < this._chart.columnsY.length; i++) {
            const column = this._chart.columnsY[i];
            this._container.appendChild(this._renderButton(column));
        }
    }

    private _renderButton(column: IChartColumn<number>): HTMLElement {
        const checkbox = createElement("input", {type: "checkbox", checked: true, className: "chartSwitch-checkbox"});

        const indicator = createElement("div", {className: "chartSwitch-indicator"}, [" "]);
        indicator.style.borderColor = column.color;
        indicator.style.backgroundColor = column.color;

        const result = createElement("label", {className: "chartSwitch-button chartSwitch-button_isEnabled"}, [
            checkbox,
            indicator,
            column.label
        ]);

        checkbox.addEventListener("change", this._toggleColumn.bind(this, result, indicator, column.name, column.color));

        return result;
    }

    private _toggleColumn(buttonElement: HTMLElement, indicatorElement: HTMLElement, name: string, color: string, e: Event): void {
        for (let i = 0; i < this._views.length; i++) {
            const view = this._views[i];
            const isEnabled = (<HTMLInputElement>e.target).checked;
            toggleClassName(buttonElement, "chartSwitch-button_isEnabled", isEnabled);
            indicatorElement.style.backgroundColor = isEnabled ? color : null;
            view.toggleColumn(name, isEnabled);
        }
    }
}
