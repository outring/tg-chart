const bufferLengthMs = 0.5 * 1000;

export class FpsCounter {

    private _drawTimes: number[] = [];

    draw(ctx: CanvasRenderingContext2D): void {
        const currentTime = new Date().valueOf();
        this._drawTimes.push(currentTime);

        let bufferTime = currentTime;
        let drawTimeIndex = this._drawTimes.length - 1;
        for (; drawTimeIndex >= 0; drawTimeIndex--) {
            bufferTime = this._drawTimes[drawTimeIndex];
            if (bufferTime < currentTime - bufferLengthMs) {
                break;
            }
        }

        const fps = Math.round((this._drawTimes.length - drawTimeIndex) / (currentTime - bufferTime) * 1000);

        ctx.save();
        ctx.font = "50px sans-serif";
        ctx.fillStyle = "#000000";
        ctx.textAlign = "right";
        ctx.textBaseline = "top";
        const padding = 10;
        ctx.fillText(fps.toString(), ctx.canvas.width - padding, padding);
        ctx.restore();
    }

}
