const monthsShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

export function formatAsMonthAndDay(date: Date): string {
    return `${monthsShort[date.getMonth()]} ${date.getDate()}`;
}
