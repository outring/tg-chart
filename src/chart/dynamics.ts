interface IDebouncedFunction {
    interval: number;
}

export function debounce<T extends (...args: any[]) => void>(callback: T, interval?: number): T & IDebouncedFunction {
    let timeout: number | undefined;

    function debounced(this: any): void {
        const context = this;
        const args = arguments;
        if (!timeout) {
            timeout = setTimeout(function () {
                timeout = undefined;
                callback.apply(context, <any>args);
            }, debounced.interval);
        }
    }

    debounced.interval = interval || 100;

    return <any>debounced;
}

let transitionIndex = 0;
let transitions: { [id: number]: number } = {};

export function startTransition(from: number, to: number, duration: number, callback: (state: number) => void): number {
    const transitionId = transitionIndex++;
    transitions[transitionId] = -1;

    const endTime = new Date().valueOf() + duration;

    function process(): void {
        if (!transitions.hasOwnProperty(transitionId)) {
            return;
        }

        const timeLeft = endTime - new Date().valueOf();
        const stateRatio = 1 - timeLeft / duration;
        const state = Math.min(from + (to - from) * stateRatio, to);

        callback(state);

        if (state < to) {
            transitions[transitionId] = requestAnimationFrame(process);
        }
        else {
            delete transitions[transitionId];
        }
    }

    requestAnimationFrame(process);

    return transitionId;
}

export function cancelTransition(transitionId: number): void {
    if (!transitions.hasOwnProperty(transitionId)) {
        return;
    }

    cancelAnimationFrame(transitions[transitionId]);
    delete transitions[transitionId];
}
