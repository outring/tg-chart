import {IChartStyle} from "./chart/chartView.js";
import {IChartNavigatorStyle} from "./chart/chartNavigator.js";

export type UiMode = "day" | "night";

interface ITheme {
    view: Partial<IChartStyle>;
    navigator: Partial<IChartNavigatorStyle>;
}

export const uiModeNames: { [mode in UiMode]: string } = {
    day: "Day Mode",
    night: "Night Mode"
};
const transitionDuration = 100;
const lineWidth = 2;
const lineColorDay = "#cccccc";
const lineColorNight = "#666666";
const fontFamily = "'Helvetica Neue', Helvetica, Arial, sans-serif";
const fontSize = 10;
const labelPaddingX = 10;
const labelPaddingY = 6;
export const themes: { [mode in UiMode]: ITheme } = {
    day: {
        view: {
            grid: {
                color: "#ecf0f3",
                width: 1,
                transitionDuration: transitionDuration
            },
            gridX: {
                width: 0
            },
            gridLabel: {
                color: "#96a2aa",
                fontFamily: fontFamily,
                fontSize: fontSize,
                padding: 0
            },
            gridLabelX: {
                padding: labelPaddingX
            },
            gridLabelY: {
                padding: labelPaddingY
            },
            line: {
                width: lineWidth,
                color: lineColorDay
            }
        },
        navigator: {
            paddingH: 0,
            paddingV: 1,
            transitionDuration: transitionDuration,
            line: {
                width: 1,
                color: lineColorDay
            },
            selection: {
                color: "rgba(221, 234, 243, 0.5)"
            },
            selectionFrame: {
                widthH: 1,
                widthV: 4,
                color: "#ddeaf3"
            }
        }
    },
    night: {
        view: {
            grid: {
                color: "#546778",
                width: 1,
                transitionDuration: transitionDuration
            },
            gridX: {
                width: 0
            },
            gridLabel: {
                color: "#546778",
                fontFamily: fontFamily,
                fontSize: fontSize,
                padding: 0
            },
            gridLabelX: {
                padding: labelPaddingX
            },
            gridLabelY: {
                padding: labelPaddingY
            },
            line: {
                width: lineWidth,
                color: lineColorNight
            }
        },
        navigator: {
            paddingH: 0,
            paddingV: 1,
            transitionDuration: transitionDuration,
            line: {
                color: lineColorNight,
                width: 1
            },
            selection: {
                color: "rgba(24, 34, 49, 0.5)"
            },
            selectionFrame: {
                widthH: 1,
                widthV: 4,
                color: "#40566b"
            }
        }
    }
};


