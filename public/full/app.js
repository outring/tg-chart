(function () {
    var defines = {};
    var entry = [null];
    function define(name, dependencies, factory) {
        defines[name] = { dependencies: dependencies, factory: factory };
        entry[0] = name;
    }
    define("require", ["exports"], function (exports) {
        Object.defineProperty(exports, "__cjsModule", { value: true });
        Object.defineProperty(exports, "default", { value: function (name) { return resolve(name); } });
    });
    define("chart/math", ["require", "exports"], function (require, exports) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        /**
         * Computes closest smaller power of 2
         * @param x
         * @param getClosestPower
         */
        function closestPow2(x, getClosestPower) {
            if (x === 0) {
                return 1;
            }
            if (x < 0) {
                throw new Error("Expected positive number");
            }
            var power = getClosestPower(Math.log(x) * Math.LOG2E);
            if (power < 0) {
                return 1;
            }
            return Math.pow(2, power);
        }
        exports.closestPow2 = closestPow2;
        /**
         * Provides max value of index range
         */
        var RangeMaxQuery = /** @class */ (function () {
            function RangeMaxQuery(values) {
                this._lastFromIndex = -1;
                this._lastToIndex = -1;
                this._lastResult = Number.MIN_VALUE;
                this._valuesCount = values.length;
                var storageSize = 2 * closestPow2(this._valuesCount, Math.ceil) - 1;
                this._maxValues = new Array(storageSize);
                this._computeValue(values, 0, 0, this._valuesCount - 1);
            }
            RangeMaxQuery.prototype.getValue = function (fromIndex, toIndex) {
                // We're querying it for the same range a lot so optimizing a bit
                if (this._lastFromIndex === fromIndex && this._lastToIndex === toIndex) {
                    return this._lastResult;
                }
                var result = this._getValue(0, 0, this._valuesCount - 1, fromIndex, toIndex);
                this._lastFromIndex = fromIndex;
                this._lastToIndex = toIndex;
                this._lastResult = result;
                return result;
            };
            RangeMaxQuery.prototype._getValue = function (storedIndex, storedFromIndex, storedToIndex, fromIndex, toIndex) {
                if (storedFromIndex > toIndex || storedToIndex < fromIndex) {
                    return Number.MIN_VALUE;
                }
                if (storedFromIndex >= fromIndex && storedToIndex <= toIndex) {
                    return this._maxValues[storedIndex];
                }
                var storedMiddleIndex = Math.floor((storedFromIndex + storedToIndex) / 2);
                return Math.max(this._getValue(storedIndex * 2 + 1, storedFromIndex, storedMiddleIndex, fromIndex, toIndex), this._getValue(storedIndex * 2 + 2, storedMiddleIndex + 1, storedToIndex, fromIndex, toIndex));
            };
            RangeMaxQuery.prototype._computeValue = function (values, storedIndex, fromIndex, toIndex) {
                var value;
                if (fromIndex === toIndex) {
                    value = values[fromIndex];
                }
                else {
                    var middleIndex = Math.floor((fromIndex + toIndex) / 2);
                    value = Math.max(this._computeValue(values, storedIndex * 2 + 1, fromIndex, middleIndex), this._computeValue(values, storedIndex * 2 + 2, middleIndex + 1, toIndex));
                }
                this._maxValues[storedIndex] = value;
                return value;
            };
            return RangeMaxQuery;
        }());
        exports.RangeMaxQuery = RangeMaxQuery;
    });
    define("chart/chart", ["require", "exports", "chart/math"], function (require, exports, math_js_1) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        var COLUMN_TYPE_X = "x";
        var COLUMN_TYPE_Y = "line";
        var COLUMN_DATA_INDEX = 1;
        var Chart = /** @class */ (function () {
            function Chart(columnX, columnsY) {
                this.columnX = columnX;
                this.columnsY = columnsY;
                this._maxValuesY = this.columnsY.map(function (x) { return new math_js_1.RangeMaxQuery(x.values); });
            }
            Chart.fromData = function (chart, convertX) {
                var columnNameX = Object.keys(chart.types).filter(function (x) { return chart.types[x] === COLUMN_TYPE_X; })[0];
                if (!columnNameX) {
                    throw new Error("Couldn't find a column type for " + COLUMN_TYPE_X);
                }
                var columnXData = chart.columns.filter(function (x) { return x[0] === columnNameX; })[0];
                if (!columnXData) {
                    throw new Error("Couldn't find a column for " + COLUMN_TYPE_X);
                }
                var valuesX = columnXData.slice(COLUMN_DATA_INDEX);
                var columnX = {
                    name: columnNameX,
                    values: convertX ? valuesX.map(convertX) : valuesX,
                    label: "",
                    color: ""
                };
                var columnsY = chart.columns
                    .filter(function (x) { return chart.types[x[0]] === COLUMN_TYPE_Y; })
                    .map(function (x) { return ({
                    name: x[0],
                    values: x.slice(COLUMN_DATA_INDEX),
                    label: chart.names[x[0]],
                    color: chart.colors[x[0]]
                }); });
                return new Chart(columnX, columnsY);
            };
            Chart.prototype.getMaxValue = function (columnIndex, fromIndex, toIndex) {
                return this._maxValuesY[columnIndex].getValue(fromIndex, toIndex);
            };
            Chart.empty = new Chart({ values: [], label: "", color: "", name: "" }, []);
            return Chart;
        }());
        exports.Chart = Chart;
    });
    define("chart/object", ["require", "exports"], function (require, exports) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        function deepExtend(destination) {
            var sources = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                sources[_i - 1] = arguments[_i];
            }
            for (var i = 0; i < sources.length; i++) {
                var source = sources[i];
                for (var property in source) {
                    if (source.hasOwnProperty(property)) {
                        destination[property] = typeof source[property] === "object"
                            ? deepExtend(destination[property] || {}, source[property])
                            : destination[property] = source[property];
                    }
                }
            }
            return destination;
        }
        exports.deepExtend = deepExtend;
        function hasProperties(object) {
            for (var property in object) {
                if (object.hasOwnProperty(property)) {
                    return true;
                }
            }
            return false;
        }
        exports.hasProperties = hasProperties;
        function deleteProperty(object, property) {
            for (var key in object) {
                if (object.hasOwnProperty(key) && key === property) {
                    delete object[key];
                    return;
                }
            }
        }
        exports.deleteProperty = deleteProperty;
    });
    define("chart/dynamics", ["require", "exports"], function (require, exports) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        function debounce(callback, interval) {
            var timeout;
            function debounced() {
                var context = this;
                var args = arguments;
                if (!timeout) {
                    timeout = setTimeout(function () {
                        timeout = undefined;
                        callback.apply(context, args);
                    }, debounced.interval);
                }
            }
            debounced.interval = interval || 100;
            return debounced;
        }
        exports.debounce = debounce;
        var transitionIndex = 0;
        var transitions = {};
        function startTransition(from, to, duration, callback) {
            var transitionId = transitionIndex++;
            transitions[transitionId] = -1;
            var endTime = new Date().valueOf() + duration;
            function process() {
                if (!transitions.hasOwnProperty(transitionId)) {
                    return;
                }
                var timeLeft = endTime - new Date().valueOf();
                var stateRatio = 1 - timeLeft / duration;
                var state = Math.min(from + (to - from) * stateRatio, to);
                callback(state);
                if (state < to) {
                    transitions[transitionId] = requestAnimationFrame(process);
                }
                else {
                    delete transitions[transitionId];
                }
            }
            requestAnimationFrame(process);
            return transitionId;
        }
        exports.startTransition = startTransition;
        function cancelTransition(transitionId) {
            if (!transitions.hasOwnProperty(transitionId)) {
                return;
            }
            cancelAnimationFrame(transitions[transitionId]);
            delete transitions[transitionId];
        }
        exports.cancelTransition = cancelTransition;
    });
    define("chart/dom", ["require", "exports", "chart/object"], function (require, exports, object_js_1) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        function createElement(tagName, props, childElements) {
            var result = document.createElement(tagName);
            if (props) {
                object_js_1.deepExtend(result, props);
            }
            if (childElements) {
                for (var i = 0; i < childElements.length; i++) {
                    var child = childElements[i];
                    if (child instanceof HTMLElement) {
                        result.appendChild(child);
                    }
                    else {
                        result.appendChild(document.createTextNode(child));
                    }
                }
            }
            return result;
        }
        exports.createElement = createElement;
        function create2DCanvas(container) {
            var canvas = createElement("canvas");
            object_js_1.deepExtend(canvas.style, {
                position: "absolute",
                width: "100%",
                height: "100%"
            });
            var position = getComputedStyle(container).position;
            if (!position || position === "static") {
                container.style.position = "relative";
            }
            container.appendChild(canvas);
            var context = canvas.getContext("2d");
            if (!context) {
                throw new Error("Couldn't create a 2D rendering context for the canvas");
            }
            return [canvas, context];
        }
        exports.create2DCanvas = create2DCanvas;
        /**
         * Applies `devicePixelRatio` to number values (even nested)
         * @param entity
         */
        function applyDensity(entity) {
            if (typeof entity === "number") {
                return (entity * (window.devicePixelRatio || 1));
            }
            if (typeof entity === "object") {
                var result = {};
                for (var property in entity) {
                    if (entity.hasOwnProperty(property)) {
                        result[property] = applyDensity(entity[property]);
                    }
                }
                return result;
            }
            if (Array.isArray(entity)) {
                return entity.map(applyDensity);
            }
            return entity;
        }
        exports.applyDensity = applyDensity;
        function addClassName(element, className) {
            if (element.classList) {
                element.classList.add(className);
                return;
            }
            var classes = element.className.split(/\s+/).filter(function (x) { return x; });
            if (classes.indexOf(className) === -1) {
                classes.push(className);
            }
            element.className = classes.join(" ");
        }
        exports.addClassName = addClassName;
        function removeClassName(element, className) {
            if (element.classList) {
                element.classList.remove(className);
                return;
            }
            var classes = element.className.split(/\s+/).filter(function (x) { return x; });
            var classIndex = classes.indexOf(className);
            if (classIndex !== -1) {
                classes.splice(classIndex, 1);
            }
            element.className = classes.join(" ");
        }
        exports.removeClassName = removeClassName;
        function toggleClassName(element, className, add) {
            if (add) {
                addClassName(element, className);
            }
            else {
                removeClassName(element, className);
            }
        }
        exports.toggleClassName = toggleClassName;
    });
    define("chart/chartSwitch", ["require", "exports", "chart/chart", "chart/dom"], function (require, exports, chart_js_1, dom_js_1) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        var ChartSwitch = /** @class */ (function () {
            function ChartSwitch() {
                this._container = dom_js_1.createElement("div", { className: "chartSwitch" });
                this._chart = chart_js_1.Chart.empty;
                this._views = [];
            }
            ChartSwitch.prototype.appendTo = function (element) {
                element.appendChild(this._container);
            };
            ChartSwitch.prototype.setChart = function (chart) {
                this._chart = chart;
                this._render();
            };
            ChartSwitch.prototype.addView = function (view) {
                this._views.push(view);
            };
            ChartSwitch.prototype._render = function () {
                for (var i = 0; i < this._container.children.length; i++) {
                    var child = this._container.children[i];
                    this._container.removeChild(child);
                }
                for (var i = 0; i < this._chart.columnsY.length; i++) {
                    var column = this._chart.columnsY[i];
                    this._container.appendChild(this._renderButton(column));
                }
            };
            ChartSwitch.prototype._renderButton = function (column) {
                var checkbox = dom_js_1.createElement("input", { type: "checkbox", checked: true, className: "chartSwitch-checkbox" });
                var indicator = dom_js_1.createElement("div", { className: "chartSwitch-indicator" }, [" "]);
                indicator.style.borderColor = column.color;
                indicator.style.backgroundColor = column.color;
                var result = dom_js_1.createElement("label", { className: "chartSwitch-button chartSwitch-button_isEnabled" }, [
                    checkbox,
                    indicator,
                    column.label
                ]);
                checkbox.addEventListener("change", this._toggleColumn.bind(this, result, indicator, column.name, column.color));
                return result;
            };
            ChartSwitch.prototype._toggleColumn = function (buttonElement, indicatorElement, name, color, e) {
                for (var i = 0; i < this._views.length; i++) {
                    var view = this._views[i];
                    var isEnabled = e.target.checked;
                    dom_js_1.toggleClassName(buttonElement, "chartSwitch-button_isEnabled", isEnabled);
                    indicatorElement.style.backgroundColor = isEnabled ? color : null;
                    view.toggleColumn(name, isEnabled);
                }
            };
            return ChartSwitch;
        }());
        exports.ChartSwitch = ChartSwitch;
    });
    define("chart/chartView", ["require", "exports", "chart/object", "chart/chart", "chart/math", "chart/dynamics", "chart/dom"], function (require, exports, object_js_2, chart_js_2, math_js_2, dynamics_js_1, dom_js_2) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        var measuresCountMin = 5;
        var measuresCountRatio = 0.25;
        var defaultOptions = {
            gridDensityX: 0.5,
            gridDensityY: 0.2
        };
        var defaultStyle = {
            grid: {
                width: 1,
                color: "#cccccc",
                transitionDuration: 100,
            },
            gridX: {},
            gridY: {},
            gridLabel: {
                padding: 5,
                fontFamily: "sans-serif",
                fontSize: 12,
                color: "#cccccc",
            },
            gridLabelX: {},
            gridLabelY: {},
            line: {
                width: 2,
                color: "#666666"
            }
        };
        function getCanvasFont(style) {
            return style.fontSize + "px " + style.fontFamily;
        }
        var ChartView = /** @class */ (function () {
            function ChartView(options, style) {
                var _a, _b;
                var _this = this;
                this._container = dom_js_2.createElement("div", { className: "chartView" });
                this._style = {};
                this._labelCacheX = {};
                this._labelCacheY = {};
                this._chart = chart_js_2.Chart.empty;
                this._minY = 0;
                this._maxY = 0;
                this._transitioningColumnsY = {};
                this._invisibleColumnsY = {};
                this._visibleAreaFrom = 0;
                this._visibleAreaTo = 1;
                this._width = 0;
                this._height = 0;
                this._gridLabelWidthX = 0;
                this._isGridDirty = false;
                this._isLinesDirty = false;
                this._gridStateX = { from: 0, to: 0, step: -1 };
                this._alternativeGridStateX = { from: 0, to: 0, step: -1 };
                this._gridStateY = { from: 0, to: 0, step: 1 };
                this._alternativeGridStateY = { from: 0, to: 0, step: 1 };
                this._transitionStateX = 0;
                this._transitionStateY = 0;
                this._transitionIdX = -1;
                this._transitionIdY = -1;
                this._transitionY = dynamics_js_1.debounce(function () {
                    var isTransitioning = _this._transitionIdY !== -1;
                    if (isTransitioning) {
                        return;
                    }
                    var hasTransitioningColumns = object_js_2.hasProperties(_this._transitioningColumnsY);
                    if (hasTransitioningColumns) {
                        _this._initializeColumnsTransition();
                    }
                    var newMaxY = _this._computeMaxY();
                    if (newMaxY === _this._maxY && !hasTransitioningColumns) {
                        return;
                    }
                    var newStep = _this._computeGridStepY(newMaxY);
                    _this._computeGridStateForStepY(_this._alternativeGridStateY, newStep);
                    var differenceY = newMaxY - _this._maxY;
                    dynamics_js_1.cancelTransition(_this._transitionIdY);
                    _this._transitionIdY = dynamics_js_1.startTransition(0, 1, _this._style.gridY.transitionDuration, function (y) {
                        _this._maxY = newMaxY - differenceY * (1 - y);
                        _this._transitionStateY = y;
                        if (y === 1) {
                            _this._transitionIdY = -1;
                            _this._computeGridStateForStepY(_this._gridStateY, _this._alternativeGridStateY.step);
                            _this._finalizeColumnsTransition();
                        }
                        else {
                            _this._computeGridStateForStepY(_this._gridStateY, _this._gridStateY.step);
                            _this._computeGridStateForStepY(_this._alternativeGridStateY, _this._alternativeGridStateY.step);
                        }
                        _this._requestRedraw();
                    });
                });
                this._updateSize = function () {
                    var newWidth = dom_js_2.applyDensity(_this._container.offsetWidth);
                    var newHeight = dom_js_2.applyDensity(_this._container.offsetHeight);
                    if (newWidth <= 0 || newHeight <= 0) {
                        throw new Error("Container is invisible");
                    }
                    if (_this._width !== newWidth) {
                        _this._gridCanvas.width = newWidth;
                        _this._linesCanvas.width = newWidth;
                        _this._width = newWidth;
                        _this._requestRedraw();
                    }
                    if (_this._height !== newHeight) {
                        _this._gridCanvas.height = newHeight;
                        _this._linesCanvas.height = newHeight;
                        _this._height = newHeight;
                        _this._requestRedraw();
                    }
                };
                this._draw = function () {
                    _this._drawGrid();
                    _this._drawLines();
                };
                this._drawGrid = function () {
                    if (!_this._isGridDirty) {
                        return;
                    }
                    _this._isGridDirty = false;
                    _this._gridContext.clearRect(0, 0, _this._width, _this._height);
                    if (_this._columnX) {
                        _this._drawGridX();
                    }
                    if (_this._columnsY.length > 0) {
                        _this._drawGridY();
                    }
                };
                this._drawLines = function () {
                    if (!_this._isLinesDirty) {
                        return;
                    }
                    _this._isLinesDirty = false;
                    var style = _this._style;
                    var ctx = _this._linesContext;
                    var isTransitioning = _this._transitionIdY !== -1;
                    ctx.clearRect(0, 0, _this._width, _this._height);
                    ctx.lineJoin = "round";
                    ctx.lineWidth = style.line.width;
                    for (var i = 0; i < _this._columnsY.length; i++) {
                        var column = _this._columnsY[i];
                        if (_this._invisibleColumnsY[column.name]) {
                            continue;
                        }
                        ctx.strokeStyle = column.color || style.line.color;
                        var columnTransitionDirection = _this._transitioningColumnsY[column.name];
                        if (columnTransitionDirection === undefined) {
                            ctx.globalAlpha = 1;
                        }
                        else if (isTransitioning) {
                            ctx.globalAlpha = columnTransitionDirection ? _this._transitionStateY : 1 - _this._transitionStateY;
                        }
                        _this._drawLine(column.values);
                    }
                };
                this._options = object_js_2.deepExtend({}, defaultOptions, options);
                this.setStyle(style || {});
                _a = dom_js_2.create2DCanvas(this._container), this._gridCanvas = _a[0], this._gridContext = _a[1];
                _b = dom_js_2.create2DCanvas(this._container), this._linesCanvas = _b[0], this._linesContext = _b[1];
            }
            Object.defineProperty(ChartView.prototype, "_columnX", {
                get: function () {
                    return this._chart.columnX;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_columnsY", {
                get: function () {
                    return this._chart.columnsY;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_top", {
                get: function () {
                    return 0;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_right", {
                get: function () {
                    return this._width;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_bottom", {
                get: function () {
                    return this._height;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_left", {
                get: function () {
                    return 0;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_chartWidth", {
                get: function () {
                    return this._chartRight - this._chartLeft;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_chartHeight", {
                get: function () {
                    return this._chartBottom - this._chartTop;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_chartRight", {
                get: function () {
                    return this._right;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_chartBottom", {
                get: function () {
                    return this._bottom - this._style.gridLabelX.fontSize - this._style.gridLabelX.padding - this._style.gridX.width;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_chartLeft", {
                get: function () {
                    return this._left + this._style.gridY.width;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_lastValueIndex", {
                get: function () {
                    return this._columnX.values.length - 1;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_visibleInitialValueIndexPrecise", {
                get: function () {
                    return this._lastValueIndex * this._visibleAreaFrom;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_visibleFinalValueIndexPrecise", {
                get: function () {
                    return this._lastValueIndex * this._visibleAreaTo;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_visibleInitialValueIndex", {
                get: function () {
                    return Math.floor(this._visibleInitialValueIndexPrecise);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_visibleFinalValueIndex", {
                get: function () {
                    return Math.ceil(this._visibleFinalValueIndexPrecise);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_chartTop", {
                get: function () {
                    return this._top + this._style.line.width / 2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_visibleLabelsCountX", {
                get: function () {
                    var visibleLabelsArea = this._options.gridDensityX * this._chartWidth;
                    return Math.ceil(visibleLabelsArea / this._gridLabelWidthX);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_visibleLabelsCountY", {
                get: function () {
                    var visibleLabelsArea = this._options.gridDensityY * this._chartHeight;
                    return Math.floor(visibleLabelsArea / this._style.gridLabelY.fontSize);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartView.prototype, "_visibleValuesCount", {
                get: function () {
                    return Math.ceil(this._visibleFinalValueIndexPrecise - this._visibleInitialValueIndexPrecise);
                },
                enumerable: true,
                configurable: true
            });
            ChartView.prototype.setChart = function (chart) {
                this._chart = chart;
                this._measure();
                this._computeX(true);
                this._computeY(true);
                this._requestRedraw();
            };
            ChartView.prototype.setVisibleArea = function (from, to, immediate) {
                if (immediate === void 0) { immediate = false; }
                if ((to - from) * this._columnX.values.length < 2) {
                    return false;
                }
                this._visibleAreaFrom = from;
                this._visibleAreaTo = to;
                this._computeX(immediate);
                this._computeY(immediate);
                this._requestRedraw();
                return true;
            };
            ChartView.prototype.appendTo = function (element) {
                element.appendChild(this._container);
                this._measure();
                this._computeX(true);
                this._computeY(true);
                window.removeEventListener("resize", this._updateSize);
                window.addEventListener("resize", this._updateSize);
                this._updateSize();
                this._requestRedraw();
            };
            ChartView.prototype._getChartXByValueIndex = function (index) {
                var indexProportion = (index - this._visibleInitialValueIndexPrecise) / (this._visibleFinalValueIndexPrecise - this._visibleInitialValueIndexPrecise);
                return this._chartLeft + this._chartWidth * indexProportion;
            };
            ChartView.prototype._getChartYByValue = function (value) {
                var valueProportion = value / this._maxY;
                return this._chartBottom - this._chartHeight * valueProportion;
            };
            ChartView.prototype._measure = function () {
                this._gridLabelWidthX = this._measureLabelsX();
            };
            ChartView.prototype._measureLabelsX = function () {
                var style = this._style;
                var ctx = this._gridContext;
                var dataValuesCount = this._columnX.values.length;
                var measuresCount = Math.max(dataValuesCount * measuresCountRatio, measuresCountMin);
                var incrementSize = Math.floor(dataValuesCount / measuresCount) || 1;
                ctx.font = getCanvasFont(style.gridLabelX);
                var result = Number.MIN_VALUE;
                for (var i = 0; i < this._columnX.values.length; i += incrementSize) {
                    var label = this._getLabelX(this._columnX.values[i]);
                    var labelWidth = ctx.measureText(label).width;
                    if (labelWidth > result) {
                        result = labelWidth;
                    }
                }
                return result;
            };
            ChartView.prototype.setStyle = function (style) {
                var newStyle = dom_js_2.applyDensity(object_js_2.deepExtend({}, defaultStyle, style));
                newStyle.gridX = object_js_2.deepExtend({}, newStyle.grid, newStyle.gridX);
                newStyle.gridY = object_js_2.deepExtend({}, newStyle.grid, newStyle.gridY);
                newStyle.gridLabelX = object_js_2.deepExtend({}, newStyle.gridLabel, newStyle.gridLabelX);
                newStyle.gridLabelY = object_js_2.deepExtend({}, newStyle.gridLabel, newStyle.gridLabelY);
                this._style = newStyle;
                this._transitionY.interval = newStyle.gridY.transitionDuration;
                this._requestRedraw();
            };
            ChartView.prototype._computeX = function (immediate) {
                if (immediate) {
                    dynamics_js_1.cancelTransition(this._transitionIdX);
                    this._transitionIdX = -1;
                    this._computeGridStateX(this._gridStateX);
                }
                else {
                    this._transitionX();
                }
            };
            ChartView.prototype._computeGridStateX = function (state) {
                var step = this._computeGridStepX();
                this._computeGridStateForStepX(state, step);
            };
            ChartView.prototype._computeGridStepX = function () {
                var visibleDivisionsCount = this._visibleValuesCount / (this._visibleLabelsCountX - 1);
                return -math_js_2.closestPow2(visibleDivisionsCount, Math.floor) || 1;
            };
            ChartView.prototype._computeGridStateForStepX = function (state, step) {
                state.step = step;
                // We don't want the rightmost label to be ever obscured so calculating the stable(!) offset
                // for the worst case scenario (full zoom-out)
                // When the `step` is less than the safe offset it's better use the step, since it's only 2 times smaller
                // which also guarantees non-obscured display
                var initialIndex = this._lastValueIndex * this._gridLabelWidthX * 0.5 / this._chartWidth;
                var labelDisplayAdjustment = Math.min(math_js_2.closestPow2(initialIndex, Math.floor), -state.step);
                // We want x-axis labels to be stable during scrolling and resizing
                var panningAdjustment = (this._visibleFinalValueIndex - this._lastValueIndex) % state.step;
                state.from = this._visibleFinalValueIndex - panningAdjustment - labelDisplayAdjustment;
                state.to = Math.max(this._visibleInitialValueIndex + state.step, 0);
            };
            ChartView.prototype._computeY = function (immediate) {
                if (immediate) {
                    dynamics_js_1.cancelTransition(this._transitionIdY);
                    this._transitionIdY = -1;
                    this._maxY = this._computeMaxY();
                    this._computeGridStateY(this._gridStateY);
                }
                else {
                    this._transitionY();
                }
            };
            ChartView.prototype._computeGridStateY = function (state) {
                var step = this._computeGridStepY(this._maxY);
                this._computeGridStateForStepY(state, step);
            };
            ChartView.prototype.toggleColumn = function (name, show) {
                this._transitioningColumnsY[name] = show;
                this._computeY(false);
            };
            ChartView.prototype._computeGridStateForStepY = function (state, step) {
                state.step = step;
                state.from = this._minY;
                state.to = Math.floor(this._maxY / step) * step;
            };
            ChartView.prototype._computeGridStepY = function (maxY) {
                var style = this._style;
                var maxValue = maxY - Math.floor(maxY * (style.gridLabelY.fontSize + style.gridLabelY.padding) / this._chartHeight);
                var visibleDivisionsCount = maxValue / (this._visibleLabelsCountY - 1);
                return Math.floor(visibleDivisionsCount) || 1;
            };
            ChartView.prototype._transitionX = function () {
                var _this = this;
                var newStep = this._computeGridStepX();
                var isTransitioning = this._transitionIdX !== -1;
                // If we're not transitioning and don't need to, just behave normally
                if (!isTransitioning && this._gridStateX.step === newStep) {
                    this._computeGridStateX(this._gridStateX);
                    return;
                }
                var isAppearing = newStep > this._gridStateX.step;
                // If we're transitioning and the step is not changed keep the current transition
                var transitioningState = isAppearing ? this._alternativeGridStateX : this._gridStateX;
                if (isTransitioning && transitioningState.step === newStep) {
                    return;
                }
                if (isAppearing) { // The new state has more divisions than the current, showing the new as an alternative
                    this._computeGridStateX(this._alternativeGridStateX);
                    this._computeGridStateForStepX(this._gridStateX, this._gridStateX.step);
                }
                else { // The new state has less divisions than the current, showing the current as an alternative
                    this._computeGridStateForStepX(this._alternativeGridStateX, this._gridStateX.step);
                    this._computeGridStateX(this._gridStateX);
                }
                dynamics_js_1.cancelTransition(this._transitionIdX);
                this._transitionIdX = dynamics_js_1.startTransition(0, 1, this._style.gridX.transitionDuration, function (x) {
                    _this._transitionStateX = isAppearing ? x : 1 - x;
                    if (x === 1) {
                        _this._transitionIdX = -1;
                        _this._computeGridStateX(_this._gridStateX);
                    }
                    else {
                        _this._computeGridStateForStepX(_this._gridStateX, _this._gridStateX.step);
                        _this._computeGridStateForStepX(_this._alternativeGridStateX, _this._alternativeGridStateX.step);
                    }
                    _this._requestGridRedraw();
                });
            };
            /**
             * Computes max Y value taking into account partially obscured
             * parts of the lines to ensure smooth scaling
             */
            ChartView.prototype._computeMaxY = function () {
                var result = Number.MIN_VALUE;
                for (var i = 0; i < this._columnsY.length; i++) {
                    var column = this._columnsY[i];
                    if (this._invisibleColumnsY[column.name] || this._transitioningColumnsY[column.name] === false) {
                        continue;
                    }
                    var values = column.values;
                    var hasObscuredLeft = this._visibleInitialValueIndex > 0;
                    var hasObscuredRight = this._visibleFinalValueIndex < values.length - 1;
                    var value = this._chart.getMaxValue(i, hasObscuredLeft ? this._visibleInitialValueIndex + 1 : this._visibleInitialValueIndex, hasObscuredRight ? this._visibleFinalValueIndex - 1 : this._visibleFinalValueIndex);
                    if (hasObscuredLeft) {
                        var neighbourIndex = Math.ceil(this._visibleInitialValueIndexPrecise);
                        var valuesDifference = values[this._visibleInitialValueIndex] - values[neighbourIndex];
                        var differenceFraction = this._visibleInitialValueIndexPrecise - this._visibleInitialValueIndex;
                        value = Math.max(value, values[this._visibleInitialValueIndex] - Math.ceil(valuesDifference * differenceFraction));
                    }
                    if (hasObscuredRight) {
                        var neighbourIndex = Math.floor(this._visibleFinalValueIndexPrecise);
                        var valuesDifference = values[this._visibleFinalValueIndex] - values[neighbourIndex];
                        var differenceFraction = this._visibleFinalValueIndex - this._visibleFinalValueIndexPrecise;
                        value = Math.max(value, values[this._visibleFinalValueIndex] - Math.ceil(valuesDifference * differenceFraction));
                    }
                    if (value > result) {
                        result = value;
                    }
                }
                return result;
            };
            ChartView.prototype._initializeColumnsTransition = function () {
                for (var columnName in this._transitioningColumnsY) {
                    if (!this._transitioningColumnsY.hasOwnProperty(columnName)) {
                        continue;
                    }
                    if (this._transitioningColumnsY[columnName]) {
                        delete this._invisibleColumnsY[columnName];
                    }
                }
            };
            ChartView.prototype._finalizeColumnsTransition = function () {
                for (var columnName in this._transitioningColumnsY) {
                    if (!this._transitioningColumnsY.hasOwnProperty(columnName)) {
                        continue;
                    }
                    if (this._transitioningColumnsY[columnName]) {
                        delete this._transitioningColumnsY[columnName];
                    }
                    else {
                        this._invisibleColumnsY[columnName] = true;
                    }
                }
            };
            ChartView.prototype._requestRedraw = function () {
                if (this._isGridDirty && this._isLinesDirty) {
                    return;
                }
                this._isGridDirty = true;
                this._isLinesDirty = true;
                requestAnimationFrame(this._draw);
            };
            ChartView.prototype._requestGridRedraw = function () {
                if (this._isGridDirty) {
                    return;
                }
                this._isGridDirty = true;
                requestAnimationFrame(this._drawGrid);
            };
            ChartView.prototype._drawGridX = function () {
                var style = this._style;
                var ctx = this._gridContext;
                var isTransitioning = this._transitionIdX !== -1;
                var hasGridLines = style.gridX.width > 0;
                ctx.font = getCanvasFont(style.gridLabelX);
                ctx.fillStyle = style.gridLabelX.color;
                ctx.textAlign = "center";
                ctx.textBaseline = "top";
                ctx.lineWidth = style.gridX.width;
                ctx.strokeStyle = style.gridX.color;
                ctx.globalAlpha = 1;
                this._drawLabelsX(this._gridStateX);
                if (hasGridLines) {
                    this._drawGridLinesX(this._gridStateX);
                }
                if (isTransitioning) {
                    ctx.globalAlpha = this._transitionStateX;
                    this._drawLabelsX(this._alternativeGridStateX);
                    if (hasGridLines) {
                        this._drawGridLinesX(this._alternativeGridStateX);
                    }
                }
            };
            ChartView.prototype._drawGridY = function () {
                var style = this._style;
                var ctx = this._gridContext;
                var isTransitioning = this._transitionIdY !== -1;
                var hasGridLines = style.gridY.width > 0;
                ctx.font = getCanvasFont(style.gridLabelY);
                ctx.fillStyle = style.gridLabelX.color;
                ctx.textAlign = "left";
                ctx.textBaseline = "bottom";
                ctx.strokeStyle = style.gridX.color;
                ctx.lineWidth = style.gridX.width;
                ctx.globalAlpha = isTransitioning ? (1 - this._transitionStateY) : 1;
                this._drawLabelsY(this._gridStateY);
                if (hasGridLines) {
                    this._drawGridLinesY(this._gridStateY);
                }
                if (isTransitioning) {
                    ctx.globalAlpha = this._transitionStateY;
                    this._drawLabelsY(this._alternativeGridStateY);
                    if (hasGridLines) {
                        this._drawGridLinesY(this._alternativeGridStateY);
                    }
                }
            };
            ChartView.prototype._drawLabelsX = function (state) {
                for (var i = state.from; i >= state.to; i += state.step) {
                    var label = this._getLabelX(this._columnX.values[i]);
                    var labelX = this._getChartXByValueIndex(i);
                    var labelY = this._chartBottom + this._style.gridLabelX.padding;
                    this._gridContext.fillText(label, labelX, labelY);
                }
            };
            ChartView.prototype._drawLabelsY = function (state) {
                for (var value = state.from; value <= state.to; value += state.step) {
                    var label = this._getLabelY(value);
                    var labelX = this._chartLeft;
                    var labelY = this._getChartYByValue(value) - this._style.gridLabelY.padding;
                    this._gridContext.fillText(label, labelX, labelY);
                }
            };
            ChartView.prototype._drawGridLinesX = function (state) {
                var ctx = this._gridContext;
                for (var i = state.from; i >= state.to; i += state.step) {
                    var gridX = this._getChartXByValueIndex(i) - this._style.gridX.width / 2;
                    ctx.beginPath();
                    ctx.moveTo(gridX, this._chartTop);
                    ctx.lineTo(gridX, this._chartBottom);
                    ctx.stroke();
                }
            };
            ChartView.prototype._drawGridLinesY = function (state) {
                var ctx = this._gridContext;
                for (var value = state.from; value <= state.to; value += state.step) {
                    var gridY = this._getChartYByValue(value) - this._style.gridY.width / 2;
                    ctx.beginPath();
                    ctx.moveTo(this._chartLeft, gridY);
                    ctx.lineTo(this._chartRight, gridY);
                    ctx.stroke();
                }
            };
            ChartView.prototype._drawLine = function (values) {
                var ctx = this._linesContext;
                ctx.beginPath();
                for (var i = this._visibleInitialValueIndex; i <= this._visibleFinalValueIndex; i++) {
                    var value = values[i];
                    var valueX = this._getChartXByValueIndex(i);
                    var valueY = this._getChartYByValue(value);
                    if (i === this._visibleInitialValueIndex) {
                        ctx.moveTo(valueX, valueY);
                    }
                    else {
                        ctx.lineTo(valueX, valueY);
                    }
                }
                ctx.stroke();
            };
            ChartView.prototype._getLabelX = function (value) {
                //Do not cache if we're not doing anything special
                var formatLabelX = this._options.formatLabelX;
                if (!formatLabelX) {
                    return value.toString();
                }
                var cacheKey = value.toString();
                return this._labelCacheX[cacheKey] ||
                    (this._labelCacheX[cacheKey] = formatLabelX(value));
            };
            ChartView.prototype._getLabelY = function (value, columnName) {
                //Do not cache if we're not doing anything special
                var formatLabelY = this._options.formatLabelY;
                if (!formatLabelY) {
                    return value.toString();
                }
                var cacheKey = value.toString();
                return this._labelCacheY[cacheKey] ||
                    (this._labelCacheY[cacheKey] = formatLabelY(value, columnName));
            };
            return ChartView;
        }());
        exports.ChartView = ChartView;
    });
    define("chart/date", ["require", "exports"], function (require, exports) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        var monthsShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        function formatAsMonthAndDay(date) {
            return monthsShort[date.getMonth()] + " " + date.getDate();
        }
        exports.formatAsMonthAndDay = formatAsMonthAndDay;
    });
    define("chart/gestureRecognizer", ["require", "exports"], function (require, exports) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        var eventTypeNames = ["PointerEvent", "TouchEvent", "MouseEvent"];
        var eventsMaps = {
            PointerEvent: {
                start: "pointerdown",
                progress: "pointermove",
                end: "pointerup"
            },
            TouchEvent: {
                start: "touchstart",
                progress: "touchmove",
                end: "touchend"
            },
            MouseEvent: {
                start: "mousedown",
                progress: "mousemove",
                end: "mouseup"
            }
        };
        var eventTypeName = eventTypeNames.filter(function (x) { return x in window; })[0];
        var eventsMap = eventsMaps[eventTypeName];
        function isValidEvent(e) {
            if (eventTypeName === "PointerEvent") {
                var pointerEvent = e;
                return pointerEvent.isPrimary;
            }
            if (eventTypeName === "TouchEvent") {
                var touchEvent = e;
                return touchEvent.changedTouches.length > 0;
            }
            return true;
        }
        function getEventXY(e) {
            if (eventTypeName === "PointerEvent") {
                var pointerEvent = e;
                return [pointerEvent.clientX, pointerEvent.clientY];
            }
            if (eventTypeName === "TouchEvent") {
                var touchEvent = e;
                return [touchEvent.changedTouches[0].clientX, touchEvent.changedTouches[0].clientY];
            }
            var mouseEvent = e;
            return [mouseEvent.clientX, mouseEvent.clientY];
        }
        var gestureTapTolerance = 5;
        var gestureEvent = {
            type: "tap",
            x: 0,
            y: 0,
            state: "indefinite",
            elementWidth: 0,
            elementHeight: 0
        };
        var GestureRecognizer = /** @class */ (function () {
            function GestureRecognizer(element) {
                var _this = this;
                this._handlers = {};
                this._initialXY = [0, 0];
                this._onTouchStart = function (e) {
                    if (_this._currentGesture || !isValidEvent(e)) {
                        return;
                    }
                    _this._currentGesture = "tap";
                    window.addEventListener(eventsMap.progress, _this._onTouchMove);
                    window.addEventListener(eventsMap.end, _this._onTouchEnd);
                    _this._initialXY = getEventXY(e);
                    _this._elementRect = _this._element.getBoundingClientRect();
                };
                this._onTouchMove = function (e) {
                    if (!isValidEvent(e)) {
                        return;
                    }
                    if (_this._currentGesture === "pan") {
                        _this._fire("pan", "indefinite", e);
                        return;
                    }
                    var eventXY = getEventXY(e);
                    if (Math.abs(eventXY[0] - _this._initialXY[0]) >= gestureTapTolerance ||
                        Math.abs(eventXY[1] - _this._initialXY[1]) >= gestureTapTolerance) {
                        _this._currentGesture = "pan";
                        _this._fire("pan", "start", e);
                    }
                };
                this._onTouchEnd = function (e) {
                    if (!_this._currentGesture || !isValidEvent(e)) {
                        return;
                    }
                    _this._fire(_this._currentGesture, _this._currentGesture === "pan" ? "end" : "indefinite", e);
                    _this._currentGesture = undefined;
                    window.removeEventListener(eventsMap.progress, _this._onTouchMove);
                    window.removeEventListener(eventsMap.end, _this._onTouchEnd);
                };
                this._element = element;
                this._elementRect = this._element.getBoundingClientRect();
                this._element.addEventListener(eventsMap.start, this._onTouchStart);
            }
            GestureRecognizer.prototype.addListener = function (type, handler) {
                var handlers = this._handlers[type] || (this._handlers[type] = []);
                handlers.push(handler);
            };
            GestureRecognizer.prototype.removeListener = function (type, handler) {
                var handlers = this._handlers[type];
                if (handlers) {
                    var handlerIndex = handlers.indexOf(handler);
                    if (handlerIndex > 0) {
                        handlers.splice(handlerIndex, 1);
                    }
                }
            };
            GestureRecognizer.prototype._fire = function (type, state, e) {
                var handlers = this._handlers[type];
                if (!handlers) {
                    return;
                }
                var eventXY = getEventXY(e);
                var x = Math.max(Math.min(eventXY[0] - this._elementRect.left, this._elementRect.width), 0);
                var y = Math.max(Math.min(eventXY[1] - this._elementRect.top, this._elementRect.height), 0);
                gestureEvent.type = type;
                gestureEvent.state = state;
                gestureEvent.x = x;
                gestureEvent.y = y;
                gestureEvent.elementWidth = this._elementRect.width;
                gestureEvent.elementHeight = this._elementRect.height;
                for (var i = 0; i < handlers.length; i++) {
                    var handler = handlers[i];
                    if (!handler(gestureEvent)) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                }
            };
            return GestureRecognizer;
        }());
        exports.GestureRecognizer = GestureRecognizer;
    });
    define("chart/chartNavigator", ["require", "exports", "chart/dom", "chart/object", "chart/chart", "chart/gestureRecognizer", "chart/dynamics"], function (require, exports, dom_js_3, object_js_3, chart_js_3, gestureRecognizer_js_1, dynamics_js_2) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        var panTolerance = 9;
        var defaultStyle = {
            paddingH: 0,
            paddingV: 1,
            transitionDuration: 100,
            line: {
                width: 1,
                color: "#666666"
            },
            selection: {
                color: "rgba(196, 196, 196, 0.5)"
            },
            selectionFrame: {
                color: "rgba(196, 196, 255, 0.75)",
                widthH: 1,
                widthV: 4
            }
        };
        var ChartNavigator = /** @class */ (function () {
            function ChartNavigator(style) {
                var _a;
                var _this = this;
                this._container = dom_js_3.createElement("div", { className: "chartNavigator" });
                this._style = {};
                this._chart = chart_js_3.Chart.empty;
                this._width = 0;
                this._height = 0;
                this._maxY = 0;
                this._transitioningColumnsY = {};
                this._invisibleColumnsY = {};
                this._selectionFrom = 0.75;
                this._selectionTo = 1;
                this._panMode = "indefinite";
                this._panDiffFrom = 0;
                this._panDiffTo = 0;
                this._isDirty = false;
                this._transitionStateY = 0;
                this._transitionIdY = -1;
                this._updateSize = function () {
                    var newWidth = dom_js_3.applyDensity(_this._container.offsetWidth);
                    var newHeight = dom_js_3.applyDensity(_this._container.offsetHeight);
                    if (newWidth <= 0 || newHeight <= 0) {
                        throw new Error("Container is invisible");
                    }
                    if (_this._width !== newWidth) {
                        _this._canvas.width = newWidth;
                        _this._width = newWidth;
                        _this._requestRedraw();
                    }
                    if (_this._height !== newHeight) {
                        _this._canvas.height = newHeight;
                        _this._height = newHeight;
                        _this._requestRedraw();
                    }
                };
                this._transitionY = dynamics_js_2.debounce(function () {
                    var isTransitioning = _this._transitionIdY !== -1;
                    if (isTransitioning) {
                        return;
                    }
                    var hasTransitioningColumns = object_js_3.hasProperties(_this._transitioningColumnsY);
                    if (hasTransitioningColumns) {
                        _this._initializeColumnsTransition();
                    }
                    var newMaxY = _this._computeMaxY();
                    if (newMaxY === _this._maxY && !hasTransitioningColumns) {
                        return;
                    }
                    var differenceY = newMaxY - _this._maxY;
                    dynamics_js_2.cancelTransition(_this._transitionIdY);
                    _this._transitionIdY = dynamics_js_2.startTransition(0, 1, _this._style.transitionDuration, function (y) {
                        _this._maxY = newMaxY - differenceY * (1 - y);
                        _this._transitionStateY = y;
                        if (y === 1) {
                            _this._transitionIdY = -1;
                            _this._finalizeColumnsTransition();
                        }
                        _this._requestRedraw();
                    });
                });
                this._onPan = function (e) {
                    var relativeX = e.x / e.elementWidth;
                    var relativeTolerance = (_this._style.selectionFrame.widthV + panTolerance) / e.elementWidth;
                    if (e.state === "start") {
                        _this._panDiffFrom = _this._selectionFrom - relativeX;
                        _this._panDiffTo = _this._selectionTo - relativeX;
                        var fromDifference = relativeX - _this._selectionFrom;
                        var toDifference = _this._selectionTo - relativeX;
                        if (fromDifference > relativeTolerance && toDifference > relativeTolerance) {
                            _this._panMode = "area";
                        }
                        else if (Math.abs(fromDifference) <= relativeTolerance) {
                            _this._panMode = "from";
                        }
                        else if (Math.abs(toDifference) <= relativeTolerance) {
                            _this._panMode = "to";
                        }
                        else {
                            _this._panMode = "indefinite";
                        }
                    }
                    if (_this._panMode === "indefinite") {
                        return true;
                    }
                    var newFrom = Math.max(relativeX + _this._panDiffFrom, 0);
                    var newTo = Math.min(relativeX + _this._panDiffTo, 1);
                    var isMinimalFrom = newFrom === _this._selectionFrom && newFrom === 0;
                    var isMinimalTo = newTo === _this._selectionTo && newTo === 1;
                    switch (_this._panMode) {
                        case "area":
                            if (isMinimalFrom || isMinimalTo) {
                                return true;
                            }
                            if (!_this._updateView(newFrom, newTo)) {
                                return true;
                            }
                            _this._selectionFrom = newFrom;
                            _this._selectionTo = newTo;
                            break;
                        case "from":
                            if (isMinimalFrom || newFrom >= _this._selectionTo) {
                                return true;
                            }
                            if (!_this._updateView(newFrom, _this._selectionTo)) {
                                return true;
                            }
                            _this._selectionFrom = newFrom;
                            break;
                        case "to":
                            if (isMinimalTo || newTo <= _this._selectionFrom) {
                                return true;
                            }
                            if (!_this._updateView(_this._selectionFrom, newTo)) {
                                return true;
                            }
                            _this._selectionTo = newTo;
                            break;
                    }
                    _this._requestRedraw();
                    return false;
                };
                this._draw = function () {
                    if (!_this._isDirty) {
                        return;
                    }
                    _this._isDirty = false;
                    _this._context.clearRect(0, 0, _this._width, _this._height);
                    _this._drawSelectionFrame();
                    _this._drawLines();
                    _this._drawSelectionOverlay();
                };
                this.setStyle(style || {});
                _a = dom_js_3.create2DCanvas(this._container), this._canvas = _a[0], this._context = _a[1];
                var gestureRecognizer = new gestureRecognizer_js_1.GestureRecognizer(this._container);
                gestureRecognizer.addListener("pan", this._onPan);
            }
            Object.defineProperty(ChartNavigator.prototype, "_columnsY", {
                get: function () {
                    return this._chart.columnsY;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_top", {
                get: function () {
                    return 0;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_right", {
                get: function () {
                    return this._width;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_bottom", {
                get: function () {
                    return this._height;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_left", {
                get: function () {
                    return 0;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_chartWidth", {
                get: function () {
                    return this._chartRight - this._chartLeft;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_chartHeight", {
                get: function () {
                    return this._chartBottom - this._chartTop;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_chartTop", {
                get: function () {
                    return this._top + this._style.paddingV + this._style.selectionFrame.widthH;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_chartRight", {
                get: function () {
                    return this._right - this._style.paddingH;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_chartBottom", {
                get: function () {
                    return this._bottom - this._style.paddingV - this._style.selectionFrame.widthH;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ChartNavigator.prototype, "_chartLeft", {
                get: function () {
                    return this._left + this._style.paddingH;
                },
                enumerable: true,
                configurable: true
            });
            ChartNavigator.prototype.setChart = function (chart) {
                this._chart = chart;
                this._maxY = this._computeMaxY();
                this._requestRedraw();
            };
            ChartNavigator.prototype.setStyle = function (style) {
                this._style = dom_js_3.applyDensity(object_js_3.deepExtend({}, defaultStyle, style));
                this._transitionY.interval = this._style.transitionDuration;
                this._requestRedraw();
            };
            ChartNavigator.prototype.appendTo = function (element) {
                element.appendChild(this._container);
                this._maxY = this._computeMaxY();
                window.removeEventListener("resize", this._updateSize);
                window.addEventListener("resize", this._updateSize);
                this._updateSize();
                this._requestRedraw();
            };
            ChartNavigator.prototype.addView = function (view) {
                this._view = view;
                this._view.setVisibleArea(this._selectionFrom, this._selectionTo, true);
            };
            ChartNavigator.prototype.toggleColumn = function (name, show) {
                this._transitioningColumnsY[name] = show;
                this._transitionY();
            };
            ChartNavigator.prototype._initializeColumnsTransition = function () {
                for (var columnName in this._transitioningColumnsY) {
                    if (!this._transitioningColumnsY.hasOwnProperty(columnName)) {
                        continue;
                    }
                    if (this._transitioningColumnsY[columnName]) {
                        delete this._invisibleColumnsY[columnName];
                    }
                }
            };
            ChartNavigator.prototype._finalizeColumnsTransition = function () {
                for (var columnName in this._transitioningColumnsY) {
                    if (!this._transitioningColumnsY.hasOwnProperty(columnName)) {
                        continue;
                    }
                    if (this._transitioningColumnsY[columnName]) {
                        delete this._transitioningColumnsY[columnName];
                    }
                    else {
                        this._invisibleColumnsY[columnName] = true;
                    }
                }
            };
            ChartNavigator.prototype._computeMaxY = function () {
                var result = Number.MIN_VALUE;
                for (var i = 0; i < this._columnsY.length; i++) {
                    var column = this._columnsY[i];
                    if (this._invisibleColumnsY[column.name] || this._transitioningColumnsY[column.name] === false) {
                        continue;
                    }
                    var value = this._chart.getMaxValue(i, 0, column.values.length - 1);
                    if (value > result) {
                        result = value;
                    }
                }
                return result;
            };
            ChartNavigator.prototype._updateView = function (from, to) {
                return this._view ? this._view.setVisibleArea(from, to) : true;
            };
            ChartNavigator.prototype._requestRedraw = function () {
                if (this._isDirty) {
                    return;
                }
                this._isDirty = true;
                requestAnimationFrame(this._draw);
            };
            ChartNavigator.prototype._drawLines = function () {
                var style = this._style;
                var ctx = this._context;
                var isTransitioning = this._transitionIdY !== -1;
                ctx.lineJoin = "round";
                ctx.lineWidth = style.line.width;
                for (var i = 0; i < this._columnsY.length; i++) {
                    var column = this._columnsY[i];
                    ctx.strokeStyle = column.color || style.line.color;
                    if (this._invisibleColumnsY[column.name]) {
                        continue;
                    }
                    ctx.strokeStyle = column.color || style.line.color;
                    var columnTransitionDirection = this._transitioningColumnsY[column.name];
                    if (columnTransitionDirection === undefined) {
                        ctx.globalAlpha = 1;
                    }
                    else if (isTransitioning) {
                        ctx.globalAlpha = columnTransitionDirection ? this._transitionStateY : 1 - this._transitionStateY;
                    }
                    this._drawLine(column.values);
                }
            };
            ChartNavigator.prototype._drawSelectionFrame = function () {
                var style = this._style;
                var ctx = this._context;
                var selectionFromEndX = this._width * this._selectionFrom;
                var selectionToStartX = this._width * this._selectionTo;
                var selectionFrameFromVX = selectionFromEndX + style.selectionFrame.widthV / 2;
                var selectionFrameToVX = selectionToStartX - style.selectionFrame.widthV / 2;
                var selectionFrameFromHX = selectionFromEndX + style.selectionFrame.widthV;
                var selectionFrameToHX = selectionToStartX - style.selectionFrame.widthV;
                ctx.strokeStyle = style.selectionFrame.color;
                ctx.globalAlpha = 1;
                ctx.lineWidth = style.selectionFrame.widthV;
                ctx.beginPath();
                ctx.moveTo(selectionFrameFromVX, this._top);
                ctx.lineTo(selectionFrameFromVX, this._bottom);
                ctx.moveTo(selectionFrameToVX, this._top);
                ctx.lineTo(selectionFrameToVX, this._bottom);
                ctx.stroke();
                ctx.lineWidth = style.selectionFrame.widthH;
                ctx.beginPath();
                ctx.moveTo(selectionFrameFromHX, this._top);
                ctx.lineTo(selectionFrameToHX, this._top);
                ctx.moveTo(selectionFrameFromHX, this._bottom);
                ctx.lineTo(selectionFrameToHX, this._bottom);
                ctx.stroke();
            };
            ChartNavigator.prototype._drawSelectionOverlay = function () {
                var style = this._style;
                var ctx = this._context;
                var selectionFromStartX = this._left;
                var selectionFromEndX = this._width * this._selectionFrom;
                var selectionToStartX = this._width * this._selectionTo;
                var selectionToEndX = this._right;
                ctx.fillStyle = style.selection.color;
                ctx.globalAlpha = 1;
                ctx.fillRect(selectionFromStartX, this._top, selectionFromEndX, this._bottom);
                ctx.fillRect(selectionToStartX, this._top, selectionToEndX, this._bottom);
            };
            ;
            ChartNavigator.prototype._drawLine = function (values) {
                var ctx = this._context;
                ctx.beginPath();
                for (var i = 0; i < values.length; i++) {
                    var value = values[i];
                    var valueX = this._chartLeft + this._chartWidth * i / (values.length - 1);
                    var valueY = this._chartTop + this._chartHeight - this._chartHeight * (value / this._maxY);
                    if (i === 0) {
                        ctx.moveTo(valueX, valueY);
                    }
                    else {
                        ctx.lineTo(valueX, valueY);
                    }
                }
                ctx.stroke();
            };
            return ChartNavigator;
        }());
        exports.ChartNavigator = ChartNavigator;
    });
    define("uiMode", ["require", "exports"], function (require, exports) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        exports.uiModeNames = {
            day: "Day Mode",
            night: "Night Mode"
        };
        var transitionDuration = 100;
        var lineWidth = 2;
        var lineColorDay = "#cccccc";
        var lineColorNight = "#666666";
        var fontFamily = "'Helvetica Neue', Helvetica, Arial, sans-serif";
        var fontSize = 10;
        var labelPaddingX = 10;
        var labelPaddingY = 6;
        exports.themes = {
            day: {
                view: {
                    grid: {
                        color: "#ecf0f3",
                        width: 1,
                        transitionDuration: transitionDuration
                    },
                    gridX: {
                        width: 0
                    },
                    gridLabel: {
                        color: "#96a2aa",
                        fontFamily: fontFamily,
                        fontSize: fontSize,
                        padding: 0
                    },
                    gridLabelX: {
                        padding: labelPaddingX
                    },
                    gridLabelY: {
                        padding: labelPaddingY
                    },
                    line: {
                        width: lineWidth,
                        color: lineColorDay
                    }
                },
                navigator: {
                    paddingH: 0,
                    paddingV: 1,
                    transitionDuration: transitionDuration,
                    line: {
                        width: 1,
                        color: lineColorDay
                    },
                    selection: {
                        color: "rgba(221, 234, 243, 0.5)"
                    },
                    selectionFrame: {
                        widthH: 1,
                        widthV: 4,
                        color: "#ddeaf3"
                    }
                }
            },
            night: {
                view: {
                    grid: {
                        color: "#546778",
                        width: 1,
                        transitionDuration: transitionDuration
                    },
                    gridX: {
                        width: 0
                    },
                    gridLabel: {
                        color: "#546778",
                        fontFamily: fontFamily,
                        fontSize: fontSize,
                        padding: 0
                    },
                    gridLabelX: {
                        padding: labelPaddingX
                    },
                    gridLabelY: {
                        padding: labelPaddingY
                    },
                    line: {
                        width: lineWidth,
                        color: lineColorNight
                    }
                },
                navigator: {
                    paddingH: 0,
                    paddingV: 1,
                    transitionDuration: transitionDuration,
                    line: {
                        color: lineColorNight,
                        width: 1
                    },
                    selection: {
                        color: "rgba(24, 34, 49, 0.5)"
                    },
                    selectionFrame: {
                        widthH: 1,
                        widthV: 4,
                        color: "#40566b"
                    }
                }
            }
        };
    });
    define("app", ["require", "exports", "chart/chart", "chart/chartView", "chart/date", "chart/dom", "chart/chartNavigator", "chart/chartSwitch", "uiMode"], function (require, exports, chart_js_4, chartView_js_1, date_js_1, dom_js_4, chartNavigator_js_1, chartSwitch_js_1, uiMode_js_1) {
        "use strict";
        Object.defineProperty(exports, "__esModule", { value: true });
        var activeUiMode = "day";
        var activeTheme = uiMode_js_1.themes[activeUiMode];
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                render(JSON.parse(xhr.responseText));
            }
        };
        xhr.open("GET", "./chart_data.json", true);
        xhr.send(null);
        function render(charts) {
            var container = document.querySelector(".app");
            var chartComponents = charts.map(function (x, i) { return renderChart(container, "Chart " + (i + 1), x); });
            renderThemeSwitch(chartComponents);
        }
        function renderThemeSwitch(chartComponents) {
            var link = dom_js_4.createElement("a", { href: "javascript:void(0)", className: "themeSwitch-link" });
            var panel = dom_js_4.createElement("div", { className: "themeSwitch" }, [link]);
            link.addEventListener("click", function () {
                setUiMode(link, chartComponents, activeUiMode === "day" ? "night" : "day");
            });
            setUiMode(link, chartComponents, activeUiMode);
            document.body.appendChild(panel);
        }
        exports.renderThemeSwitch = renderThemeSwitch;
        function renderChart(appContainer, chartName, chartData) {
            var container = dom_js_4.createElement("div", { className: "chart" }, [
                dom_js_4.createElement("div", { className: "chart-heading" }, [chartName])
            ]);
            appContainer.appendChild(container);
            var chart = chart_js_4.Chart.fromData(chartData, function (x) { return new Date(x); });
            var view = new chartView_js_1.ChartView({
                gridDensityX: 0.3,
                gridDensityY: 0.2,
                formatLabelX: date_js_1.formatAsMonthAndDay
            }, activeTheme.view);
            view.appendTo(container);
            view.setChart(chart);
            var navigator = new chartNavigator_js_1.ChartNavigator(activeTheme.navigator);
            navigator.appendTo(container);
            navigator.setChart(chart);
            navigator.addView(view);
            var chartSwitch = new chartSwitch_js_1.ChartSwitch();
            chartSwitch.appendTo(container);
            chartSwitch.setChart(chart);
            chartSwitch.addView(view);
            chartSwitch.addView(navigator);
            return [view, navigator];
        }
        function setUiMode(link, chartComponents, uiMode) {
            dom_js_4.addClassName(document.documentElement, "theme");
            dom_js_4.removeClassName(document.documentElement, "theme_" + activeUiMode);
            dom_js_4.addClassName(document.documentElement, "theme_" + uiMode);
            activeUiMode = uiMode;
            activeTheme = uiMode_js_1.themes[activeUiMode];
            link.innerText = "Switch to " + uiMode_js_1.uiModeNames[activeUiMode === "day" ? "night" : "day"];
            setTimeout(function () {
                chartComponents.forEach(function (x) {
                    x[0].setStyle(activeTheme.view);
                    x[1].setStyle(activeTheme.navigator);
                });
            }, 100);
        }
    });
    //# sourceMappingURL=app.js.map
    'marker:resolver';

    function get_define(name) {
        if (defines[name]) {
            return defines[name];
        }
        else if (defines[name + '/index']) {
            return defines[name + '/index'];
        }
        else {
            var dependencies = ['exports'];
            var factory = function (exports) {
                try {
                    Object.defineProperty(exports, "__cjsModule", { value: true });
                    Object.defineProperty(exports, "default", { value: require(name) });
                }
                catch (_a) {
                    throw Error(['module "', name, '" not found.'].join(''));
                }
            };
            return { dependencies: dependencies, factory: factory };
        }
    }
    var instances = {};
    function resolve(name) {
        if (instances[name]) {
            return instances[name];
        }
        if (name === 'exports') {
            return {};
        }
        var define = get_define(name);
        instances[name] = {};
        var dependencies = define.dependencies.map(function (name) { return resolve(name); });
        define.factory.apply(define, dependencies);
        var exports = dependencies[define.dependencies.indexOf('exports')];
        instances[name] = (exports['__cjsModule']) ? exports.default : exports;
        return instances[name];
    }
    if (entry[0] !== null) {
        return resolve(entry[0]);
    }
})();
//# sourceMappingURL=app.js.map
